#pragma once

namespace sr
{
	enum class BufferType
	{
		Color = 0,
		Depth = 1
	};
}
