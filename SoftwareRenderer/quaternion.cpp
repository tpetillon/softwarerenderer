#include "quaternion.h"

#include "vector3.h"
#include "vector4.h"
#include "matrix4x4.h"

#include <cmath>

namespace sr
{
	Quaternion::Quaternion() : w(1), x(0), y(0), z(0)
	{
	}

	Quaternion::Quaternion(float w, float x, float y, float z)
		: w(w), x(x), y(y), z(z)
	{
		normalize();
	}

	Quaternion Quaternion::operator*(const Quaternion& q) const
	{
		return Quaternion(
			q.w * w - q.x * x - q.y * y - q.z * z,
			q.w * x + q.x * w - q.y * z + q.z * y,
			q.w * y + q.x * z + q.y * w - q.z * x,
			q.w * z - q.x * y + q.y * x + q.z * w);
	}

	Vector3 Quaternion::operator*(const Vector3& v) const
	{
		return (toRotationMatrix() * Vector4(v.x, v.y, v.z, 0)).toVector3();
	}

	Quaternion Quaternion::inverse() const
	{
		// return the conjugate, this works because the quaternion is normalised
		return Quaternion(w, -x, -y, -z);
	}

	Matrix4x4 Quaternion::toRotationMatrix() const
	{
		// simplified calculation, because the quaternion is normalised
		// cf. https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Quaternion-derived_rotation_matrix

		return Matrix4x4(
			1 - 2*y*y - 2*z*z, 2*x*y - 2*w*z,     2*x*z + 2*w*y,     0,
			2*x*y + 2*w*z,     1 - 2*x*x - 2*z*z, 2*y*z - 2*w*x,     0,
			2*x*z - 2*w*y,     2*y*z + 2*w*x,     1 - 2*x*x - 2*y*y, 0,
			0,                 0,                 0,                 1);
	}

	Vector3 Quaternion::toEulerAngles() const
	{
		// cf. https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Conversion
		return Vector3(
			atan2(2 * (w * x + y * z), 1 - 2 * (x * x + y * y)),
			asin(2 * (w * y - x * z)),
			atan2(2 * (w * z + x * y), 1 - 2 * (y * y + z * z)));
	}

	Quaternion Quaternion::angleAxis(float angle, Vector3 axis)
	{
		axis = axis.normalized();

		float s = sin(angle / 2);

		return Quaternion(cos(angle / 2), axis.x * s, axis.y * s, axis.z * s);
	}

	Quaternion Quaternion::fromEulerAngles(float x, float y, float z)
	{
		// yaw-pitch-roll (Tait-Bryan angles)
		// intrinsic rotation on z axis, then y axis, then x axis
		// equivalent to extrinsic rotation on x axis, then y axis, then z axis
		// cf. https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Conversion

		float c1 = cos(x / 2);
		float c2 = cos(y / 2);
		float c3 = cos(z / 2);
		float s1 = sin(x / 2);
		float s2 = sin(y / 2);
		float s3 = sin(z / 2);

		return Quaternion(
			c1 * c2 * c3 + s1 * s2 * s3,
			s1 * c2 * c3 - c1 * s2 * s3,
			c1 * s2 * c3 + s1 * c2 * s3,
			c1 * c2 * s3 - s1 * s2 * c3);
	}

	void Quaternion::normalize()
	{
		float norm = sqrt(w*w + x*x + y*y + z*z);

		w = w / norm;
		x = x / norm;
		y = y / norm;
		z = z / norm;
	}

	const Quaternion Quaternion::identity = Quaternion(0, 0, 0, 1);
}
