#pragma once

namespace sr
{
	class Vector2;

	class Vector2i
	{
	public:
		int x;
		int y;

		Vector2i();
		Vector2i(int x, int y);
		Vector2i(Vector2 v);

		int sqrtMagnitude() const;

		bool operator==(const Vector2i& v) const;
		bool operator!=(const Vector2i& v) const;

		Vector2i operator+(const Vector2i& v) const;
		Vector2i operator-(const Vector2i& v) const;

		Vector2i& operator+=(const Vector2i& v);

		int dot(const Vector2i& v) const;
		int cross(const Vector2i& v) const;

		static const Vector2i zero;
		static const Vector2i one;
	};

	Vector2i operator*(const Vector2i& v, int i);
	Vector2i operator*(int i, const Vector2i& v);
}
