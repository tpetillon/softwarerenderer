#pragma once

namespace sr
{
	enum class MeshType
	{
		Lines,
		Triangles
	};
}
