#include "math.h"

#include <algorithm>

namespace sr
{
	namespace Math
	{
		float clamp(float v, float min, float max)
		{
			return std::min(std::max(min, v), max);
		}

		float clamp01(float v)
		{
			return std::min(std::max(0.0f, v), 1.0f);
		}
	}
}
