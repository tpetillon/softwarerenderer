#pragma once

#include <memory>

#include "vector2i.h"
#include "vector3.h"
#include "matrix4x4.h"
#include "quaternion.h"
#include "rendertarget.h"

namespace sr
{
	class Camera
	{
	public:
		Camera(
			int width, int height, Vector3 position, Quaternion rotation,
			float fov, float nearPlaneDistance, float farPlaneDistance,
			std::unique_ptr<RenderTarget> renderTarget);

		RenderTarget& getRenderTarget() const { return *renderTarget; }

		const Matrix4x4& getTransform() const { return transform; }
		const Matrix4x4& getViewTransform() const { return viewTransform; }
		const Matrix4x4& getProjection() const { return projection; }
		const Matrix4x4& getViewProjection() const { return viewProjection; }

		Vector2i getSize() const { return size; }
		float getAspectRatio() const { return aspectRatio; }
		float getFoV() const { return cameraFoV; }
		float getHorizontalFoV() const { return horizontalFoV; }

		void setPosition(Vector3 position);
		void setRotation(Quaternion rotation);
		void translate(Vector3 translation);
		void rotate(Quaternion rotation, bool inWorldSpace);

		Vector3 getPosition() const { return position; }
		Quaternion getRotation() const { return rotation; }
		Vector3 getForward() const { return forward; }

		Vector3 transformWorldToViewport(Vector3 position) const;
		Vector2 transformViewportToScreen(Vector3 position) const;
		Vector2i transformViewportToScreen(Vector3 position, int subpixelPrecision) const;

		bool clipped(int x, int y) const;

	private:
		int cameraWidth;
		int cameraHeight;
		Vector2i size;

		float nearPlaneDistance;
		float farPlaneDistance;

		float aspectRatio; // width / height

		float cameraFoV; // vertical angle of view
		float horizontalFoV;

		Vector3 position;
		Quaternion rotation;

		Matrix4x4 transform;
		Matrix4x4 viewTransform; // includes rotation for z on depth axis
		Matrix4x4 projection;
		Matrix4x4 viewProjection;

		Vector3 forward;

		std::unique_ptr<RenderTarget> renderTarget;

		void computeTransform();
		void computeViewTransform();
		void computeProjectionMatrix();
		void computeForwardVector();
	};
}
