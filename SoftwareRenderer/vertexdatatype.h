#pragma once

namespace sr
{
	enum class VertexDataType
	{
		Position,
		Color,
		TexCoord,
		Normal
	};
}
