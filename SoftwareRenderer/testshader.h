#pragma once

#include "shader.h"

#include "vector2.h"
#include "vector3.h"
#include "color.h"
#include "matrix4x4.h"

namespace sr
{
	class Vector4;
	class TextureSampler;

	struct TestShaderUniforms
	{
		Matrix4x4 modelViewTransform;
		TextureSampler* texture0;
		Vector3 lightDirection;
		float lightIntensity;
		float ambiantLightIntensity;
	};

	struct TestShaderVaryings
	{
		Vector3 position;
		Color color;
		Vector2 texCoord;
		Vector3 normal;
	};

	class TestShader : public ShaderImpl<TestShaderUniforms, TestShaderVaryings>
	{
	public:
		TestShader();

		Vector4 transformVertex(TestShaderVaryings* input) override;
		Color colorFragment(TestShaderVaryings* input) override;
	};
}
