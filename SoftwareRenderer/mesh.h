#pragma once

#include <map>
#include <memory>
#include <vector>

#include "meshtype.h"
#include "vertexdatatype.h"
#include "vertexdataarray.h"

namespace sr
{
	class Mesh
	{
	public:
		Mesh(MeshType meshType);

		MeshType getType() const { return meshType; }

		void setIndices(std::vector<int> indices);

		const std::vector<int>& getIndices() const { return indices; }

		void addData(VertexDataType type, std::unique_ptr<IVertexDataArray> dataArray);

		template <typename T>
		const VertexDataArray<T>& getData(VertexDataType type) const
		{
			return static_cast<const VertexDataArray<T>&>(*(dataArrays.at(type)));
		}

		void calculateNormals();

	private:
		MeshType meshType;
		std::vector<int> indices;
		std::map<VertexDataType, std::unique_ptr<IVertexDataArray>> dataArrays;
	};
}
