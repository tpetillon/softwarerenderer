#pragma once

#include <memory>
#include <string>

namespace sr
{
	class Mesh;

	class ObjLoader
	{
	public:
		static std::unique_ptr<Mesh> loadFile(std::string fileName, bool swapYZ, bool invertWindingDirection);
	};
}
