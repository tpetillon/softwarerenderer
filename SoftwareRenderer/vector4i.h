#pragma once

namespace sr
{
	class Vector4;

	class Vector4i
	{
	public:
		int x;
		int y;
		int z;
		int w;

		Vector4i();
		Vector4i(int x, int y, int z, int w);
		Vector4i(Vector4 v);

		int sqrtMagnitude() const;

		bool operator==(const Vector4i& v) const;
		bool operator!=(const Vector4i& v) const;

		Vector4i operator+(const Vector4i& v) const;
		Vector4i operator-(const Vector4i& v) const;

		Vector4i operator+(int i) const;
		Vector4i operator/(int i) const;

		Vector4i& operator+=(const Vector4i& v);
		Vector4i& operator+=(int i);

		Vector4i operator|(const Vector4i& v);

		int dot(const Vector4i& v) const;

		static const Vector4i zero;
		static const Vector4i one;
	};

	Vector4i operator*(const Vector4i& v, int i);
	Vector4i operator*(int i, const Vector4i& v);
}
