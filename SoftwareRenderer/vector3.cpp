#include "vector3.h"

#include "math.h"

#include <cmath>

namespace sr
{
	using Math::clamp01;

	Vector3::Vector3() : Vector3(0, 0, 0)
	{
	}

	Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z)
	{
	}

	float Vector3::magnitude() const
	{
		return std::sqrt(sqrtMagnitude());
	}

	float Vector3::sqrtMagnitude() const
	{
		return x * x + y * y + z * z;
	}

	Vector3 Vector3::normalized() const
	{
		return (*this) / magnitude();
	}

	bool Vector3::operator==(const Vector3& v) const
	{
		return x == v.x && y == v.y && z == v.z;
	}

	bool Vector3::operator!=(const Vector3& v) const
	{
		return !(*this == v);
	}

	Vector3 Vector3::operator+(const Vector3& v) const
	{
		return Vector3(x + v.x, y + v.y, z + v.z);
	}

	Vector3 Vector3::operator-(const Vector3& v) const
	{
		return Vector3(x - v.x, y - v.y, z - v.z);
	}

	Vector3 Vector3::operator/(float f) const
	{
		return Vector3(x / f, y / f, z / f);
	}

	Vector3 operator*(const Vector3& v, float f)
	{
		return Vector3(v.x * f, v.y * f, v.z * f);
	}

	Vector3 operator*(float f, const Vector3& v)
	{
		return v * f;
	}

	Vector3& Vector3::operator+=(const Vector3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}

	float Vector3::dot(const Vector3& v) const
	{
		return x * v.x + y * v.y + z * v.z;
	}

	Vector3 Vector3::cross(const Vector3& v) const
	{
		return Vector3(
			y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x);
	}

	Vector3 Vector3::lerp(const Vector3& v0, const Vector3& v1, float progression)
	{
		float p = clamp01(progression);
		return (1 - p) * v0 + p * v1;
	}

	const Vector3 Vector3::zero = Vector3(0.0f, 0.0f, 0.0f);
	const Vector3 Vector3::one = Vector3(1.0f, 1.0f, 1.0f);
}
