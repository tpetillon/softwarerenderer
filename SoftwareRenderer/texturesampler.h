#pragma once

#include "color.h"

namespace sr
{
	enum class SamplingType;

	class TextureSampler
	{
	public:
		TextureSampler(int width, int height, Color* data, SamplingType samplingType);
		~TextureSampler();

		Color getColor(float u, float v) const;

	private:
		int width;
		int height;
		float fwidth;
		float fheight;

		Color* data;
		SamplingType samplingType;
	};
}
