#include <SDL.h>
#include <stdio.h>
#include <cstdint>
#include <memory>
#include <string>

#include "lodepng.h"

#include "color.h"
#include "scene.h"
#include "texturesampler.h"
#include "samplingtype.h"
#include "objloader.h"
#include "mesh.h"
#include "matrix4x4.h"
#include "vector3.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

Uint32 convertColor(sr::Color color)
{
	return (uint8_t(color.a * 255) << (8 * 3)) +
		(uint8_t(color.r * 255) << (8 * 2)) +
		(uint8_t(color.g * 255) << 8) +
		uint8_t(color.b * 255);
}

sr::Color convertColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return sr::Color(
		float(r) / 255.0f,
		float(g) / 255.0f,
		float(b) / 255.0f,
		float(a) / 255.0f);
}

void initAndRunRenderer(SDL_Window* window, std::string objPath, std::string texturePath, float modelScale)
{
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

	SDL_Texture* screenTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_WIDTH, SCREEN_HEIGHT);

	uint32_t* screenPixels = new uint32_t[SCREEN_WIDTH * SCREEN_HEIGHT];

	std::unique_ptr<sr::Mesh> mesh = sr::ObjLoader::loadFile(objPath, true, true);

	std::vector<unsigned char> sourceTexturePixels;
	uint32_t textureWidth, textureHeight;
	uint32_t error = lodepng::decode(sourceTexturePixels, textureWidth, textureHeight, texturePath.c_str());

	if (error)
	{
		printf("Could not load texture %s: %s\n", texturePath.c_str(), lodepng_error_text(error));
		return;
	}

	sr::Color* texturePixels = new sr::Color[textureWidth * textureHeight];
	for (uint32_t y = 0; y < textureHeight; y++)
	{
		for (uint32_t x = 0; x < textureWidth; x++)
		{
			// PNG (0,0) is top-left, this renderer's (0,0) is bottom-left.
			// The texture is vertically inverted on load to counter that.
			int index = x + y * textureWidth;
			int invertedIndex = x + (textureHeight - y - 1) * textureWidth;
			auto color = convertColor(
				sourceTexturePixels[index * 4 + 0],
				sourceTexturePixels[index * 4 + 1],
				sourceTexturePixels[index * 4 + 2],
				sourceTexturePixels[index * 4 + 3]);
			texturePixels[invertedIndex] = color;
		}
	}
	std::unique_ptr<sr::TextureSampler> sampler = std::make_unique<sr::TextureSampler>(
		textureWidth, textureHeight, texturePixels, sr::SamplingType::NearestNeighbor);

	auto modelTransform = sr::Matrix4x4::scale(sr::Vector3(modelScale, modelScale, modelScale));

	sr::Scene scene(SCREEN_WIDTH, SCREEN_HEIGHT, std::move(mesh), modelTransform, std::move(sampler));

	bool quit = false;
	SDL_Event event;

	uint32_t lastTime = SDL_GetTicks();

	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;

	bool trackMouse = false;

	while (!quit)
	{
		uint32_t time = SDL_GetTicks();
		uint32_t elaspedTime = time - lastTime;
		lastTime = time;

		int mouseMotionX = 0;
		int mouseMotionY = 0;

		//Handle events on queue
		while (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.state == SDL_PRESSED)
				{
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
						quit = true;
						break;
					case SDLK_UP:
						up = true;
						break;
					case SDLK_DOWN:
						down = true;
						break;
					case SDLK_LEFT:
						left = true;
						break;
					case SDLK_RIGHT:
						right = true;
						break;
					}
				}
			}
			else if (event.type == SDL_KEYUP)
			{
				if (event.key.state == SDL_RELEASED)
				{
					switch (event.key.keysym.sym)
					{
					case SDLK_UP:
						up = false;
						break;
					case SDLK_DOWN:
						down = false;
						break;
					case SDLK_LEFT:
						left = false;
						break;
					case SDLK_RIGHT:
						right = false;
						break;
					}
				}
			}
			else if (event.type == SDL_MOUSEMOTION)
			{
				if (trackMouse)
				{
					mouseMotionX = event.motion.xrel;
					mouseMotionY = event.motion.yrel;
				}
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				SDL_SetRelativeMouseMode(SDL_TRUE);
				trackMouse = true;
			}
			else if (event.type == SDL_MOUSEBUTTONUP)
			{
				SDL_SetRelativeMouseMode(SDL_FALSE);
				trackMouse = false;
			}
		}

		scene.update(
			float(lastTime) / 1000.0f, float(elaspedTime) / 1000.0f,
			up, down, left, right, mouseMotionX, mouseMotionY);

		scene.render();

		auto& colorBuffer = scene.getPixels();
		for (int y = 0; y < SCREEN_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_WIDTH; x++)
			{
				screenPixels[x + SCREEN_WIDTH * (SCREEN_HEIGHT - y - 1)] = convertColor(colorBuffer.get(x, y));
			}
		}

		//pixels[x + SCREEN_WIDTH * y] = 0;

		//x = (x + 1) % SCREEN_WIDTH;
		//if (x == 0)
		//{
		//	y = (y + 1) % SCREEN_HEIGHT;
		//}

		SDL_UpdateTexture(screenTexture, nullptr, screenPixels, SCREEN_WIDTH * sizeof(uint32_t));

		SDL_RenderCopy(renderer, screenTexture, nullptr, nullptr);
		SDL_RenderPresent(renderer);
	}

	delete[] screenPixels;
	SDL_DestroyTexture(screenTexture);
	SDL_DestroyRenderer(renderer);
}

void printUsage(char* args[])
{
	auto executablePath = std::string(args[0]);
	auto lastSlashPos = executablePath.find_last_of('\\');
	if (lastSlashPos == std::string::npos)
	{
		lastSlashPos = -1;
	}
	else
	{
		lastSlashPos++;
	}
	auto executableName = executablePath.substr(lastSlashPos);
	printf("Usage: %s <obj file path> <texture file path> [<scale>]\n", executableName.c_str());
}

int main(int argc, char* args[])
{
	if (argc < 3)
	{
		printUsage(args);
		return 1;
	}

	float modelScale = 1.0f;
	if (argc >= 4)
	{
		try
		{
			modelScale = std::stof(args[3]);
		}
		catch (...)
		{
			printUsage(args);
			return 1;
		}
	}

	//The window we'll be rendering to
	SDL_Window* window = nullptr;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//Create window
		window = SDL_CreateWindow("Software Renderer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == nullptr)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			initAndRunRenderer(window, std::string(args[1]), std::string(args[2]), modelScale);
		}
	}

	//Destroy window
	SDL_DestroyWindow(window);

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
