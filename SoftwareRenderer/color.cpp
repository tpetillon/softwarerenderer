#include "color.h"

#include "math.h"

namespace sr
{
	using Math::clamp01;

	Color::Color() : Color(0, 0, 0, 0)
	{
	}

	Color::Color(float r, float g, float b, float a) : r(clamp01(r)), g(clamp01(g)), b(clamp01(b)), a(clamp01(a))
	{
	}

	Color::Color(float r, float g, float b) : Color(r, g, b, 1)
	{
	}

	bool Color::operator==(const Color& c) const
	{
		return r == c.r && g == c.g && b == c.b && a == c.a;
	}

	bool Color::operator!=(const Color& c) const
	{
		return !(*this == c);
	}

	Color Color::operator+(const Color& c) const
	{
		return Color(r + c.r, g + c.g, b + c.b, a + c.a);
	}

	Color Color::operator-(const Color& c) const
	{
		return Color(r - c.r, g - c.g, b - c.b, a - c.a);
	}

	Color Color::operator*(const Color& c) const
	{
		return Color(r * c.r, g * c.g, b * c.b, a * c.a);
	}

	Color Color::operator/(float f) const
	{
		return Color(r / f, g / f, b / f, a / f);
	}

	Color operator*(const Color& c, float f)
	{
		return Color(c.r * f, c.g * f, c.b * f, c.a * f);
	}

	Color operator*(float f, const Color& c)
	{
		return c * f;
	}

	Color Color::lerp(const Color& c0, const Color& c1, float progression)
	{
		float p = clamp01(progression);
		return (1 - p) * c0 + p * c1;
	}

	const Color Color::white = Color(1.0f, 1.0f, 1.0f, 1.0f);
	const Color Color::black = Color(0.0f, 0.0f, 0.0f, 1.0f);
	const Color Color::red = Color(1.0f, 0.0f, 0.0f, 1.0f);
	const Color Color::green = Color(0.0f, 1.0f, 0.0f, 1.0f);
	const Color Color::blue = Color(0.0f, 0.0f, 1.0f, 1.0f);
	const Color Color::yellow = Color(1.0f, 1.0f, 0.0f, 1.0f);
	const Color Color::cyan = Color(0.0f, 1.0f, 1.0f, 1.0f);
	const Color Color::magenta = Color(1.0f, 0.0f, 1.0f, 1.0f);
	const Color Color::grey = Color(0.5f, 0.5f, 0.5f, 1.0f);
}
