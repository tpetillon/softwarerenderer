#pragma once

namespace sr
{
	class Vector2
	{
	public:
		float x;
		float y;

		Vector2();
		Vector2(float x, float y);

		float magnitude() const;
		float sqrtMagnitude() const;

		Vector2 normalized() const;

		bool operator==(const Vector2& v) const;
		bool operator!=(const Vector2& v) const;

		Vector2 operator+(const Vector2& v) const;
		Vector2 operator-(const Vector2& v) const;

		Vector2 operator/(float f) const;

		Vector2& operator+=(const Vector2& v);

		float dot(const Vector2& v) const;
		float cross(const Vector2& v) const;

		static Vector2 lerp(const Vector2& v0, const Vector2& v1, float progression);

		static const Vector2 zero;
		static const Vector2 one;
	};

	Vector2 operator*(const Vector2& v, float f);
	Vector2 operator*(float f, const Vector2& v);
}
