#include "vector4.h"

#include "vector3.h"
#include "vector4i.h"

#include <cmath>

namespace sr
{
	Vector4::Vector4() : Vector4(0, 0, 0, 0)
	{
	}

	Vector4::Vector4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w)
	{
	}

	Vector4::Vector4(Vector4i v) : x(float(v.x)), y(float(v.y)), z(float(v.z)), w(float(v.w))
	{
	}

	float Vector4::magnitude() const
	{
		return std::sqrt(sqrtMagnitude());
	}

	float Vector4::sqrtMagnitude() const
	{
		return x * x + y * y + z * z + w * w;
	}

	Vector4 Vector4::normalized() const
	{
		return (*this) / magnitude();
	}

	Vector3 Vector4::toVector3() const
	{
		return Vector3(x, y, z);
	}

	bool Vector4::operator==(const Vector4& v) const
	{
		return x == v.x && y == v.y && z == v.z && w == v.w;
	}

	bool Vector4::operator!=(const Vector4& v) const
	{
		return !(*this == v);
	}

	Vector4 Vector4::operator+(const Vector4& v) const
	{
		return Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	Vector4 Vector4::operator-(const Vector4& v) const
	{
		return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	Vector4 Vector4::operator/(float f) const
	{
		return Vector4(x / f, y / f, z / f, w / f);
	}

	Vector4 operator*(const Vector4& v, float f)
	{
		return Vector4(v.x * f, v.y * f, v.z * f, v.w * f);
	}

	Vector4 operator*(float f, const Vector4& v)
	{
		return v * f;
	}

	Vector4& Vector4::operator+=(const Vector4& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;

		return *this;
	}

	float Vector4::dot(const Vector4& v) const
	{
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	const Vector4 Vector4::zero = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
	const Vector4 Vector4::one = Vector4(1.0f, 1.0f, 1.0f, 1.0f);

	Vector4 Vector4::lerp(const Vector4& v0, const Vector4& v1, float t)
	{
		return v0 * (1 - t) + v1 * t;
	}
}
