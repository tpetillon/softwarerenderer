#pragma once

#include <memory>
#include <map>

#include "buffertype.h"
#include "buffer.h"

namespace sr
{
	class RenderTarget
	{
	public:
		RenderTarget(std::map<BufferType, std::unique_ptr<IBuffer>> buffers);

		template <typename T>
		Buffer<T>& getBuffer(BufferType bufferType)
		{
			return static_cast<Buffer<T>&>(*buffers.at(bufferType));
		}

		template <typename T>
		void write(BufferType bufferType, size_t x, size_t y, T value)
		{
			static_cast<Buffer<T>>(buffers[bufferType])->set(x, y, value);
		}

		template <typename T>
		const T read(BufferType bufferType, size_t x, size_t y) const
		{
			static_cast<Buffer<T>>(buffers[bufferType])->get(x, y);
		}

	private:
		const std::map<BufferType, std::unique_ptr<IBuffer>> buffers;
	};
}
