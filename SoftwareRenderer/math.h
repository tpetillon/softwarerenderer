#pragma once

namespace sr
{
	namespace Math
	{
		float clamp(float v, float min, float max);
		float clamp01(float v);

		const float PI = 3.14159265358979323846f;
	}
}
