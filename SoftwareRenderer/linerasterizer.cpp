#include "linerasterizer.h"

#include "mesh.h"
#include "camera.h"
#include "meshtype.h"
#include "vertexdatatype.h"
#include "vector2.h"
#include "vector3.h"
#include "vector4.h"
#include "color.h"
#include "math.h"

#include <algorithm>
#include <cmath>

namespace sr
{
	using std::swap;
	using Math::clamp01;

	void LineRasterizer::render(Mesh& mesh, Camera& camera) const
	{
		if (mesh.getType() != MeshType::Lines)
		{
			// Error, mesh type not supported
			return;
		}

		for (size_t i = 0; i < mesh.getIndices().size() / 2; i++)
		{
			renderLine(i, mesh, camera);
		}
	}

	void LineRasterizer::renderLine(int lineIndex, Mesh& mesh, Camera& camera) const
	{
		int i0 = mesh.getIndices()[lineIndex * 2];
		int i1 = mesh.getIndices()[lineIndex * 2 + 1];

		auto p0 = mesh.getData<Vector3>(VertexDataType::Position)[i0];
		auto p1 = mesh.getData<Vector3>(VertexDataType::Position)[i1];

		auto p0vp = camera.transformWorldToViewport(p0);
		auto p1vp = camera.transformWorldToViewport(p1);

		auto p0s = camera.transformViewportToScreen(p0vp);
		auto p1s = camera.transformViewportToScreen(p1vp);

		// Bresenham's line algorithm
		float x0 = p0s.x;
		float y0 = p0s.y;
		float x1 = p1s.x;
		float y1 = p1s.y;
		const bool steep = (abs(y1 - y0) > abs(x1 - x0));

		if (steep)
		{
			swap(x0, y0);
			swap(x1, y1);
		}

		const bool invert = x0 > x1;

		if (invert)
		{
			swap(x0, x1);
			swap(y0, y1);
		}

		const float dx = x1 - x0;
		const float dy = abs(y1 - y0);

		float error = dx / 2.0f;
		const int ystep = (y0 < y1) ? 1 : -1;
		int y = (int) y0;

		const int maxX = (int) x1;

		for (int x = (int) x0; x < maxX; x++)
		{
			float linearProgression = clamp01((x - x0) / (x1 - x0));
			if (invert)
			{
				linearProgression = 1 - linearProgression;
			}

			float perpectiveProgression = (linearProgression / p1vp.z) / ((1 - linearProgression) / p0vp.z + linearProgression / p1vp.z);

			if (steep)
			{
				drawPoint(y, x, p0vp, p1vp, i0, i1, perpectiveProgression, mesh, camera);
			}
			else
			{
				drawPoint(x, y, p0vp, p1vp, i0, i1, perpectiveProgression, mesh, camera);
			}

			error -= dy;
			if (error < 0)
			{
				y += ystep;
				error += dx;
			}
		}
	}

	void LineRasterizer::drawPoint(int x, int y, Vector3 p0vp, Vector3 p1vp, int i0, int i1, float interpolation, Mesh& mesh, Camera& camera) const
	{
		if (camera.clipped(x, y))
		{
			return;
		}

		auto depth = (1 - interpolation) * p0vp.z + interpolation * p1vp.z;

		if (depth < -1 || depth > 1)
		{
			return;
		}

		auto depthBufferValue = camera.getRenderTarget().getBuffer<float>(BufferType::Depth).get(x, y);

		if (depth > depthBufferValue)
		{
			return;
		}

		camera.getRenderTarget().getBuffer<float>(BufferType::Depth).set(x, y, depth);

		auto color = Color::lerp(
			mesh.getData<Color>(VertexDataType::Color)[i0],
			mesh.getData<Color>(VertexDataType::Color)[i1],
			interpolation);

		camera.getRenderTarget().getBuffer<Color>(BufferType::Color).set(x, y, color);
	}
}
