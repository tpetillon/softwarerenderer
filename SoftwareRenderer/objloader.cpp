#include "objloader.h"

#include <algorithm>
#include <fstream>
#include <unordered_map>
#include <sstream>
#include <vector>

#include "mesh.h"
#include "meshtype.h"
#include "vector2.h"
#include "vector3.h"

namespace sr
{
	using std::ifstream;
	using std::istringstream;
	using std::make_unique;
	using std::move;
	using std::string;
	using std::unique_ptr;
	using std::unordered_map;
	using std::vector;

	struct VertexIndices
	{
		int positionIndex;
		int normalIndex;
		int texcoordsIndex;

		VertexIndices(int positionIndex, int normalIndex, int texcoordsIndex)
			: positionIndex(positionIndex), normalIndex(normalIndex), texcoordsIndex(texcoordsIndex)
		{}

		bool operator==(const VertexIndices& other) const
		{
			return
				positionIndex == other.positionIndex &&
				normalIndex == other.normalIndex &&
				texcoordsIndex == other.texcoordsIndex;
		}
	};

	// cf. http://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key
	struct VertexIndicesHasher
	{
		std::size_t operator()(const VertexIndices& v) const
		{
			using std::size_t;
			using std::hash;
			using std::string;

			return
				((hash<int>()(v.positionIndex) ^
				(hash<int>()(v.normalIndex) << 1)) >> 1) ^
				(hash<int>()(v.texcoordsIndex) << 1);
		}
	};

	unique_ptr<Mesh> ObjLoader::loadFile(string fileName, bool swapYZ, bool invertWindingDirection)
	{
		auto mesh = make_unique<Mesh>(MeshType::Triangles);

		vector<Vector3> rawPositions;
		vector<Vector3> rawNormals;
		vector<Vector2> rawTexcoords;

		vector<int> indices;
		vector<VertexIndices> vertices;
		unordered_map<VertexIndices, int, VertexIndicesHasher> vertexMap;

		bool addNormals = false;
		bool addTexcoords = false;

		ifstream infile(fileName);
		string line;
		while (std::getline(infile, line))
		{
			istringstream iss(line);
			string lineType;
			if (!(iss >> lineType))
			{
				// empty line
				continue;
			}

			//printf("line type: %s", lineType);

			if (lineType[0] == '#')
			{
				// comment
				continue;
			}
			else if (lineType == "v")
			{
				// vertex position
				float x, y, z;
				iss >> x >> y >> z;

				if (swapYZ)
				{
					rawPositions.push_back(Vector3(x, z, y));
				}
				else
				{
					rawPositions.push_back(Vector3(x, y, z));
				}
			}
			else if (lineType == "vn")
			{
				// normal
				float x, y, z;
				iss >> x >> y >> z;

				rawNormals.push_back(Vector3(x, y, z));
			}
			else if (lineType == "vt")
			{
				// texture coordinates
				float u, v;
				iss >> u >> v;

				rawTexcoords.push_back(Vector2(u, v));
			}
			else if (lineType == "f")
			{
				// face
				auto slashCount = std::count(line.begin(), line.end(), '/');
				auto doubleSlash = line.find("//");

				int p0 = 0; int p1 = 0; int p2 = 0;
				int tc0 = 0; int tc1 = 0; int tc2 = 0;
				int n0 = 0; int n1 = 0; int n2 = 0;
				char slash;

				if (slashCount == 6)
				{
					addNormals = true;

					if (doubleSlash == std::string::npos)
					{
						// f pos/texcoord/norm pos/texcoord/norm pos/texcoord/norm
						iss >> p0 >> slash >> tc0 >> slash >> n0
							>> p1 >> slash >> tc1 >> slash >> n1
							>> p2 >> slash >> tc2 >> slash >> n2;

						addTexcoords = true;
					}
					else
					{
						// f pos//norm pos//norm pos//norm
						iss >> p0 >> slash >> slash >> n0
							>> p1 >> slash >> slash >> n1
							>> p2 >> slash >> slash >> n2;
					}
				}
				else if (slashCount == 3)
				{
					// f pos/texcoord pos/texcoord pos/texcoord
					iss >> p0 >> slash >> tc0
						>> p1 >> slash >> tc1
						>> p2 >> slash >> tc2;

					addTexcoords = true;
				}
				else
				{
					// f pos pos pos
					iss >> p0
						>> p1
						>> p2;
				}

				if (invertWindingDirection)
				{
					std::swap(p1, p2);
					std::swap(n1, n2);
					std::swap(tc1, tc2);
				}

				auto vertexIndices0 = VertexIndices(p0, n0, tc0);
				auto vertexIndices1 = VertexIndices(p1, n1, tc1);
				auto vertexIndices2 = VertexIndices(p2, n2, tc2);

				auto search = vertexMap.find(vertexIndices0);
				if (search != vertexMap.end())
				{
					indices.push_back(search->second);
				}
				else
				{
					int index = vertices.size();
					vertices.push_back(vertexIndices0);
					vertexMap[vertexIndices0] = index;

					indices.push_back(index);
				}

				search = vertexMap.find(vertexIndices1);
				if (search != vertexMap.end())
				{
					indices.push_back(search->second);
				}
				else
				{
					int index = vertices.size();
					vertices.push_back(vertexIndices1);
					vertexMap[vertexIndices1] = index;

					indices.push_back(index);
				}

				search = vertexMap.find(vertexIndices2);
				if (search != vertexMap.end())
				{
					indices.push_back(search->second);
				}
				else
				{
					int index = vertices.size();
					vertices.push_back(vertexIndices2);
					vertexMap[vertexIndices2] = index;

					indices.push_back(index);
				}
			}
		}

		printf("%i positions, %i normals, %i texcoords loaded.\n",
			rawPositions.size(), rawNormals.size(), rawTexcoords.size());

		auto positions = vector<Vector3>(vertices.size());
		auto normals = vector<Vector3>(rawNormals.size() != 0 ? vertices.size() : 0);
		auto texcoords = vector<Vector2>(vertices.size());

		for (unsigned int i = 0; i < vertices.size(); i++)
		{
			positions[i] = rawPositions[vertices[i].positionIndex - 1];
		}

		if (rawNormals.size() != 0)
		{
			for (unsigned int i = 0; i < vertices.size(); i++)
			{
				int normalIndex = vertices[i].normalIndex - 1;

				if (normalIndex != -1)
				{
					normals[i] = rawNormals[normalIndex];
				}
				else
				{
					normals[i] = Vector3::zero;
				}
			}
		}
		if (rawTexcoords.size() != 0)
		{
			for (unsigned int i = 0; i < vertices.size(); i++)
			{
				int texCoordIndex = vertices[i].texcoordsIndex - 1;

				if (texCoordIndex != -1)
				{
					texcoords[i] = rawTexcoords[texCoordIndex];
				}
				else
				{
					texcoords[i] = Vector2::zero;
				}
			}
		}

		mesh->setIndices(move(indices));

		auto positionDataArray = make_unique<VertexDataArray<Vector3>>(positions);
		mesh->addData(VertexDataType::Position, static_cast<unique_ptr<IVertexDataArray>>(move(positionDataArray)));

		if (normals.size() != 0)
		{
			auto normalDataArray = make_unique<VertexDataArray<Vector3>>(normals);
			mesh->addData(VertexDataType::Normal, static_cast<unique_ptr<IVertexDataArray>>(move(normalDataArray)));
		}
		else
		{
			mesh->calculateNormals();
		}

		auto texcoordDataArray = make_unique<VertexDataArray<Vector2>>(texcoords);
		mesh->addData(VertexDataType::TexCoord, static_cast<unique_ptr<IVertexDataArray>>(move(texcoordDataArray)));

		return move(mesh);
	}
}
