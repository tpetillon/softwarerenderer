#include "matrix4x4.h"

#include "vector3.h"
#include "vector4.h"
#include "quaternion.h"

#include <cmath>

using std::cos;
using std::sin;

namespace sr
{
	Matrix4x4::Matrix4x4() :
		v({
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		})
	{
	}

	Matrix4x4::Matrix4x4(
		float v00, float v01, float v02, float v03,
		float v10, float v11, float v12, float v13,
		float v20, float v21, float v22, float v23,
		float v30, float v31, float v32, float v33) :
		v({
			v00, v01, v02, v03,
			v10, v11, v12, v13,
			v20, v21, v22, v23,
			v30, v31, v32, v33
		})
	{
	}

	float Matrix4x4::operator()(unsigned row, unsigned col) const
	{
		return v[row * 4 + col];
	}

	Matrix4x4 Matrix4x4::operator*(const Matrix4x4& m) const
	{
		const Matrix4x4& t = *this;
		return Matrix4x4(
			// first line
			t(0, 0) * m(0, 0) + t(0, 1) * m(1, 0) + t(0, 2) * m(2, 0) + t(0, 3) * m(3, 0),
			t(0, 0) * m(0, 1) + t(0, 1) * m(1, 1) + t(0, 2) * m(2, 1) + t(0, 3) * m(3, 1),
			t(0, 0) * m(0, 2) + t(0, 1) * m(1, 2) + t(0, 2) * m(2, 2) + t(0, 3) * m(3, 2),
			t(0, 0) * m(0, 3) + t(0, 1) * m(1, 3) + t(0, 2) * m(2, 3) + t(0, 3) * m(3, 3),
			// second line
			t(1, 0) * m(0, 0) + t(1, 1) * m(1, 0) + t(1, 2) * m(2, 0) + t(1, 3) * m(3, 0),
			t(1, 0) * m(0, 1) + t(1, 1) * m(1, 1) + t(1, 2) * m(2, 1) + t(1, 3) * m(3, 1),
			t(1, 0) * m(0, 2) + t(1, 1) * m(1, 2) + t(1, 2) * m(2, 2) + t(1, 3) * m(3, 2),
			t(1, 0) * m(0, 3) + t(1, 1) * m(1, 3) + t(1, 2) * m(2, 3) + t(1, 3) * m(3, 3),
			// third line
			t(2, 0) * m(0, 0) + t(2, 1) * m(1, 0) + t(2, 2) * m(2, 0) + t(2, 3) * m(3, 0),
			t(2, 0) * m(0, 1) + t(2, 1) * m(1, 1) + t(2, 2) * m(2, 1) + t(2, 3) * m(3, 1),
			t(2, 0) * m(0, 2) + t(2, 1) * m(1, 2) + t(2, 2) * m(2, 2) + t(2, 3) * m(3, 2),
			t(2, 0) * m(0, 3) + t(2, 1) * m(1, 3) + t(2, 2) * m(2, 3) + t(2, 3) * m(3, 3),
			// fourth line
			t(3, 0) * m(0, 0) + t(3, 1) * m(1, 0) + t(3, 2) * m(2, 0) + t(3, 3) * m(3, 0),
			t(3, 0) * m(0, 1) + t(3, 1) * m(1, 1) + t(3, 2) * m(2, 1) + t(3, 3) * m(3, 1),
			t(3, 0) * m(0, 2) + t(3, 1) * m(1, 2) + t(3, 2) * m(2, 2) + t(3, 3) * m(3, 2),
			t(3, 0) * m(0, 3) + t(3, 1) * m(1, 3) + t(3, 2) * m(2, 3) + t(3, 3) * m(3, 3));
	}

	Vector4 Matrix4x4::operator*(const Vector3& v) const
	{
		const Matrix4x4& t = *this;

		return Vector4(
			t(0, 0) * v.x + t(0, 1) * v.y + t(0, 2) * v.z + t(0, 3),
			t(1, 0) * v.x + t(1, 1) * v.y + t(1, 2) * v.z + t(1, 3),
			t(2, 0) * v.x + t(2, 1) * v.y + t(2, 2) * v.z + t(2, 3),
			t(3, 0) * v.x + t(3, 1) * v.y + t(3, 2) * v.z + t(3, 3));
	}

	Vector4 Matrix4x4::operator*(const Vector4& v) const
	{
		const Matrix4x4& t = *this;

		return Vector4(
			t(0, 0) * v.x + t(0, 1) * v.y + t(0, 2) * v.z + t(0, 3) * v.w,
			t(1, 0) * v.x + t(1, 1) * v.y + t(1, 2) * v.z + t(1, 3) * v.w,
			t(2, 0) * v.x + t(2, 1) * v.y + t(2, 2) * v.z + t(2, 3) * v.w,
			t(3, 0) * v.x + t(3, 1) * v.y + t(3, 2) * v.z + t(3, 3) * v.w);
	}

	Vector3 Matrix4x4::multiplyDirection(const Vector3& v) const
	{
		const Matrix4x4& t = *this;

		return Vector3(
			t(0, 0) * v.x + t(0, 1) * v.y + t(0, 2) * v.z,
			t(1, 0) * v.x + t(1, 1) * v.y + t(1, 2) * v.z,
			t(2, 0) * v.x + t(2, 1) * v.y + t(2, 2) * v.z);
	}

	const Matrix4x4 Matrix4x4::identity = Matrix4x4(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	Matrix4x4 Matrix4x4::translation(Vector3 translation)
	{
		return Matrix4x4(
			1, 0, 0, translation.x,
			0, 1, 0, translation.y,
			0, 0, 1, translation.z,
			0, 0, 0, 1);
	}

	Matrix4x4 Matrix4x4::rotation(Vector3 eulerAngles)
	{
		// yaw-pitch-roll (Tait-Bryan angles)
		// intrinsic rotation on z axis, then y axis, then x axis
		// equivalent to extrinsic rotation on x axis, then y axis, then z axis
		// cf. http://www.songho.ca/opengl/gl_anglestoaxes.html

		const float cosA = cos(eulerAngles.x);
		const float cosB = cos(eulerAngles.y);
		const float cosC = cos(eulerAngles.z);

		const float sinA = sin(eulerAngles.x);
		const float sinB = sin(eulerAngles.y);
		const float sinC = sin(eulerAngles.z);

		// rotZ(c) * rotY(b) * rotX(a);
		return Matrix4x4(
			cosB * cosC, sinA * sinB * cosC - cosA * sinC, cosA * sinB * cosC + sinA * sinC, 0,
			cosB * sinC, sinA * sinB * sinC + cosA * cosC, cosA * sinB * sinC - sinA * cosC, 0,
			-sinB, sinA * cosB, cosA * cosB, 0,
			0, 0, 0, 1);
	}

	Matrix4x4 Matrix4x4::scale(Vector3 scale)
	{
		return Matrix4x4(
			scale.x, 0, 0, 0,
			0, scale.y, 0, 0,
			0, 0, scale.z, 0,
			0, 0, 0, 1);
	}

	Matrix4x4 Matrix4x4::transform(Vector3 translation, Quaternion rotation, Vector3 scale)
	{
		return Matrix4x4::translation(translation) * rotation.toRotationMatrix() * Matrix4x4::scale(scale);
	}
}
