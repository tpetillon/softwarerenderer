#include "mesh.h"

#include <memory>
#include <vector>

#include "vector3.h"

namespace sr
{
	using std::make_unique;
	using std::move;
	using std::unique_ptr;
	using std::vector;

	Mesh::Mesh(MeshType meshType) : meshType(meshType)
	{
	}

	void Mesh::setIndices(std::vector<int> indices)
	{
		this->indices = move(indices);
	}

	void Mesh::addData(VertexDataType type, std::unique_ptr<IVertexDataArray> dataArray)
	{
		dataArrays[type] = move(dataArray);
	}

	void Mesh::calculateNormals()
	{
		auto& positions = getData<Vector3>(VertexDataType::Position);
		auto normals = vector<Vector3>(positions.getData().size());

		unsigned int indexCount = indices.size();

		for (unsigned int i = 0; i < indexCount; i += 3)
		{
			auto i0 = indices[i + 0];
			auto i1 = indices[i + 1];
			auto i2 = indices[i + 2];

			auto p0 = positions[i0];
			auto p1 = positions[i1];
			auto p2 = positions[i2];

			auto p0p1 = (p0 - p1).normalized();
			auto p0p2 = (p0 - p2).normalized();

			auto normal = p0p1.cross(p0p2);

			normals[i0] += normal;
			normals[i1] += normal;
			normals[i2] += normal;
		}

		for (unsigned int i = 0; i < normals.size(); i++)
		{
			normals[i] = normals[i].normalized();
		}

		auto normalDataArray = make_unique<VertexDataArray<Vector3>>(normals);
		addData(VertexDataType::Normal, static_cast<unique_ptr<IVertexDataArray>>(move(normalDataArray)));
	}
}
