#include "vector2.h"

#include "math.h"

#include <cmath>

namespace sr
{
	using Math::clamp01;

	Vector2::Vector2() : Vector2(0, 0)
	{
	}

	Vector2::Vector2(float x, float y) : x(x), y(y)
	{
	}

	float Vector2::magnitude() const
	{
		return std::sqrt(sqrtMagnitude());
	}

	float Vector2::sqrtMagnitude() const
	{
		return x * x + y * y;
	}

	Vector2 Vector2::normalized() const
	{
		return (*this) / magnitude();
	}

	bool Vector2::operator==(const Vector2& v) const
	{
		return x == v.x && y == v.y;
	}

	bool Vector2::operator!=(const Vector2& v) const
	{
		return !(*this == v);
	}

	Vector2 Vector2::operator+(const Vector2& v) const
	{
		return Vector2(x + v.x, y + v.y);
	}

	Vector2 Vector2::operator-(const Vector2& v) const
	{
		return Vector2(x - v.x, y - v.y);
	}

	Vector2 Vector2::operator/(float f) const
	{
		return Vector2(x / f, y / f);
	}

	Vector2 operator*(const Vector2& v, float f)
	{
		return Vector2(v.x * f, v.y * f);
	}

	Vector2 operator*(float f, const Vector2& v)
	{
		return v * f;
	}

	Vector2& Vector2::operator+=(const Vector2& v)
	{
		x += v.x;
		y += v.y;

		return *this;
	}

	float Vector2::dot(const Vector2& v) const
	{
		return x * v.x + y * v.y;
	}

	float Vector2::cross(const Vector2& v) const
	{
		return x * v.y - y * v.x;
	}

	Vector2 Vector2::lerp(const Vector2& v0, const Vector2& v1, float progression)
	{
		float p = clamp01(progression);
		return (1 - p) * v0 + p * v1;
	}

	const Vector2 Vector2::zero = Vector2(0.0f, 0.0f);
	const Vector2 Vector2::one = Vector2(1.0f, 1.0f);
}
