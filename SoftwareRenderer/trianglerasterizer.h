#pragma once

#include "vector2i.h"
#include "vector4.h"
#include "vector4i.h"

#include <cstdint>

namespace sr
{
	class Mesh;
	class Camera;
	class Shader;
	class Vector2;
	class Vector3;
	class Matrix4x4;

	class TriangleRasterizer
	{
	public:
		TriangleRasterizer();

		void render(Mesh& mesh, const Matrix4x4& transform, Camera& camera, Shader& shader) const;

	private:
		int subpixelPrecision;
		Vector4i pixelGroupPatternX;
		Vector4i pixelGroupPatternY;
		Vector2i pixelGroupSize;

		struct VertexWeights
		{
			float w0, w1, w2;

			VertexWeights() = default;
			VertexWeights(float w0, float w1, float w2);
		};

		struct TriangleData
		{
			void *var0, *var1, *var2;
			Vector4 p0, p1, p2;

			TriangleData() = default;
		};

		struct Edge
		{
			int xIncrement;
			int yIncrement;
			Vector4i minPWeight;

			Edge(
				const Vector2i& p0, const Vector2i& p1, const Vector2i& minP, int subpixelPrecision,
				const Vector4i& pixelGroupPatternX, const Vector4i& pixelGroupPatternY, const Vector2i& pixelGroupSize);
		};

		void renderTriangle(int triangleIndex, Mesh& mesh, Camera& camera, Shader& shader) const;
		void getVaryings(int vertexIndex, void* varyingStruct, Mesh& mesh, Camera& camera, Shader& shader) const;
		void* interpolateVaryings(void* var0, void* var1, void* newVar, float progression, Mesh& mesh, Shader& shader) const;
		void* interpolateVaryings(
			void* var0, void* var1, void* var2, void* newVar,
			float weight0, float weight1, float weight2,
			Mesh& mesh, Shader& shader) const;
		bool clipTriangle(Vector4 p0, Vector4 p1, Vector4 p2, TriangleData& data0, TriangleData& data1, Mesh& mesh, Shader& shader) const;
		void renderClippedTriangle(
			TriangleData& data, int i0, int i1, int i2,
			Mesh& mesh, Camera& camera, Shader& shader) const;
		float clipVertex(Vector4 p0, Vector4 p1) const;
		void drawPoints(
			Vector4i mask,
			Vector4i x, Vector4i y,
			Vector4 p0, Vector4 p1, Vector4 p2,
			float z0, float z1, float z2,
			int i0, int i1, int i2,
			Vector4 weight0, Vector4 weight1, Vector4 weight2,
			TriangleData& data,
			Mesh& mesh, Camera& camera, Shader& shader) const;
		void drawPoint(
			int x, int y,
			Vector4 p0, Vector4 p1, Vector4 p2,
			float z0, float z1, float z2,
			int i0, int i1, int i2,
			float weight0, float weight1, float weight2,
			TriangleData& data,
			Mesh& mesh, Camera& camera, Shader& shader) const;

		static Vector3 perspectiveDivide(const Vector4& v);
		static bool inViewFrustum(const Vector4& p0, const Vector4& p1, const Vector4& p2);
		static int64_t parallelogramArea(const Vector2i& p0, const Vector2i& p1, const Vector2i& p2);
		static bool isTopLeftEdge(const Vector2i& p0, const Vector2i& p1);
		static bool hasNonNegativeComponent(const Vector4i& v);
	};
}
