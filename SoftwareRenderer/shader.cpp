#include "shader.h"

#include "vector4.h"
#include "color.h"
#include "vertexdatatype.h"

namespace sr
{
	void Shader::setUniformOffset(std::string uniform, ptrdiff_t offset)
	{
		uniformOffsets[uniform] = offset;
	}

	void Shader::setVaryingOffset(VertexDataType type, ptrdiff_t offset)
	{
		varyingOffsets[type] = offset;
	}
}
