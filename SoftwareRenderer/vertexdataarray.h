#pragma once

#include <vector>

namespace sr
{
	class IVertexDataArray
	{
	};

	template <typename T>
	class VertexDataArray : public IVertexDataArray
	{
	public:
		VertexDataArray(std::vector<T> data) : data(std::move(data))
		{
		}
		
		VertexDataArray(const VertexDataArray& other) = delete;

		VertexDataArray& operator=(const VertexDataArray& other) = delete;

		const std::vector<T>& getData() const
		{
			return data;
		}

		const T& operator[](const int& index) const
		{
			return data[index];
		}

	private:
		std::vector<T> data;
	};
}
