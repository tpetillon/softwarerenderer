#pragma once

#include "vector4.h" // Why doesn't the predeclaration work and is this include necessary?
#include "color.h" // same comment

#include <string>
#include <map>
#include <vector>
#include <array>

namespace sr
{
	//class Vector4;
	//class Color;
	enum class VertexDataType;

	class Shader
	{
	public:
		struct Varying
		{
			VertexDataType type;
			ptrdiff_t offset;
		};

		Shader()
		{
		}

		template <typename T>
		void setUniform(std::string name, T value)
		{
			char* uniforms = static_cast<char*>(getUniformStruct());

			auto uniformAndOffset = uniformOffsets.find(name);

			if (uniformAndOffset == uniformOffsets.end())
			{
				return;
			}

			ptrdiff_t offset = uniformAndOffset->second;

			T* address = (T*)(uniforms + offset);
			*address = value;
		}
		
		virtual Vector4 transformVertex(void* varyings) = 0;
		virtual Color colorFragment(void* varyings) = 0;

		virtual void* getVaryingStruct(size_t index) = 0;
		
		const std::map<VertexDataType, ptrdiff_t>& getVaryingOffsets() const
		{
			return varyingOffsets;
		}

	protected:
		virtual void* getUniformStruct() = 0;

		void setUniformOffset(std::string uniform, ptrdiff_t offset);
		void setVaryingOffset(VertexDataType type, ptrdiff_t offset);

	private:
		std::map<std::string, ptrdiff_t> uniformOffsets;
		std::map<VertexDataType, ptrdiff_t> varyingOffsets;
	};

	template <typename TUniforms, typename TVaryings>
	class ShaderImpl : public Shader
	{
	public:
		ShaderImpl() : Shader()
		{
		}

		void* getVaryingStruct(size_t index) override
		{
			return &(varyings[index]);
		}

	protected:
		TUniforms uniforms;
		std::array<TVaryings, 6> varyings;

		void* getUniformStruct() override
		{
			return &uniforms;
		}

		TVaryings* getVaryings()
		{
			return (TVaryings*)varyings;
		}

		Vector4 transformVertex(void* varyings)
		{
			return transformVertex((TVaryings*)varyings);
		}

		virtual Vector4 transformVertex(TVaryings* varyings) = 0;

		Color colorFragment(void* varyings)
		{
			return colorFragment((TVaryings*)varyings);
		}
		
		virtual Color colorFragment(TVaryings* varyings) = 0;
	};
}
