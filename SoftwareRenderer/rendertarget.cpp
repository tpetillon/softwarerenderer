#include "rendertarget.h"

#include "buffertype.h"
#include "buffer.h"

namespace sr
{
	RenderTarget::RenderTarget(std::map<BufferType, std::unique_ptr<IBuffer>> buffers)
		: buffers(std::move(buffers))
	{
	}
}
