#include "scene.h"

#include <vector>
#include <map>
#include <cmath>
#include <iostream>

#include "vector3.h"
#include "vector4.h"
#include "color.h"
#include "buffer.h"
#include "buffertype.h"
#include "rendertarget.h"
#include "camera.h"
#include "meshtype.h"
#include "vertexdataarray.h"
#include "math.h"
#include "shader.h"
#include "testshader.h"

namespace sr
{
	using std::unique_ptr;
	using std::make_unique;
	using std::move;
	using std::map;
	using std::vector;
	using std::max;

	Scene::Scene(int screenWidth, int screenHeight,
		unique_ptr<Mesh> mesh, Matrix4x4 modelTransform, unique_ptr<TextureSampler> texture)
		: clearColor(Color(0.2f, 0.2f, 0.4f, 1)), cubeMesh(MeshType::Triangles), planeMesh(MeshType::Triangles),
		  mesh(move(mesh)), modelTransform(modelTransform), texture(move(texture))
	{
		unique_ptr<IBuffer> colorBuffer = make_unique<Buffer<Color>>(screenWidth, screenHeight);
		unique_ptr<IBuffer> depthBuffer = make_unique<Buffer<float>>(screenWidth, screenHeight);
		map<BufferType, unique_ptr<IBuffer>> buffers;
		buffers[BufferType::Color] = move(colorBuffer);
		buffers[BufferType::Depth] = move(depthBuffer);
		auto renderTarget = make_unique<RenderTarget>(move(buffers));

		Vector3 cameraPosition{ 30, 5, 15 };
		Quaternion cameraRotation = Quaternion::fromEulerAngles(-Math::PI / 8, 0, Math::PI / 2);

		camera = make_unique<Camera>(
			screenWidth, screenHeight,
			cameraPosition, cameraRotation,
			Math::PI / 3, 1.0f, 200.0f,
			move(renderTarget));

		initCubeMesh();
		initPlaneMesh();

		shader = static_cast<unique_ptr<Shader>>(make_unique<TestShader>());
		shader->setUniform<TextureSampler*>("texture0", this->texture.get());
		shader->setUniform<float>("lightIntensity", 10);
		shader->setUniform<float>("ambiantLightIntensity", 0.5f);
	}

	void Scene::initCubeMesh()
	{
		vector<Vector3> positions
		{
			// -z
			Vector3(0, 0, 0),
			Vector3(0, 10, 0),
			Vector3(10, 10, 0),
			Vector3(10, 0, 0),

			// -y
			Vector3(0, 0, 0),
			Vector3(10, 0, 0),
			Vector3(10, 0, 10),
			Vector3(0, 0, 10),

			// +x
			Vector3(10, 0, 0),
			Vector3(10, 10, 0),
			Vector3(10, 10, 10),
			Vector3(10, 0, 10),

			// +y
			Vector3(10, 10, 0),
			Vector3(0, 10, 0),
			Vector3(0, 10, 10),
			Vector3(10, 10, 10),

			// -x
			Vector3(0, 10, 0),
			Vector3(0, 0, 0),
			Vector3(0, 0, 10),
			Vector3(0, 10, 10),

			// +z
			Vector3(0, 0, 10),
			Vector3(10, 0, 10),
			Vector3(10, 10, 10),
			Vector3(0, 10, 10)
		};
		auto positionDataArray = make_unique<VertexDataArray<Vector3>>(positions);
		cubeMesh.addData(VertexDataType::Position, static_cast<unique_ptr<IVertexDataArray>>(move(positionDataArray)));

		vector<Vector2> texcoords
		{
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1),
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1),
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1),
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1),
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1),
			Vector2(0, 0),
			Vector2(1, 0),
			Vector2(1, 1),
			Vector2(0, 1)
		};
		auto texcoordDataArray = make_unique<VertexDataArray<Vector2>>(texcoords);
		cubeMesh.addData(VertexDataType::TexCoord, static_cast<unique_ptr<IVertexDataArray>>(move(texcoordDataArray)));

		vector<Color> colors
		{
			Color::black,
			Color::green,
			Color::yellow,
			Color::red,
			Color::black,
			Color::red,
			Color::magenta,
			Color::blue,
			Color::red,
			Color::yellow,
			Color::white,
			Color::magenta,
			Color::yellow,
			Color::green,
			Color::cyan,
			Color::white,
			Color::green,
			Color::black,
			Color::blue,
			Color::cyan,
			Color::blue,
			Color::magenta,
			Color::white,
			Color::cyan
		};
		auto colorDataArray = make_unique<VertexDataArray<Color>>(colors);
		cubeMesh.addData(VertexDataType::Color, static_cast<unique_ptr<IVertexDataArray>>(move(colorDataArray)));

		vector<Vector3> normals
		{
			Vector3(0, 0, -1),
			Vector3(0, 0, -1),
			Vector3(0, 0, -1),
			Vector3(0, 0, -1),
			Vector3(0, -1, 0),
			Vector3(0, -1, 0),
			Vector3(0, -1, 0),
			Vector3(0, -1, 0),
			Vector3(1, 0, 0),
			Vector3(1, 0, 0),
			Vector3(1, 0, 0),
			Vector3(1, 0, 0),
			Vector3(0, 1, 0),
			Vector3(0, 1, 0),
			Vector3(0, 1, 0),
			Vector3(0, 1, 0),
			Vector3(-1, 0, 0),
			Vector3(-1, 0, 0),
			Vector3(-1, 0, 0),
			Vector3(-1, 0, 0),
			Vector3(0, 0, 1),
			Vector3(0, 0, 1),
			Vector3(0, 0, 1),
			Vector3(0, 0, 1)
		};
		auto normalDataArray = make_unique<VertexDataArray<Vector3>>(normals);
		cubeMesh.addData(VertexDataType::Normal, static_cast<unique_ptr<IVertexDataArray>>(move(normalDataArray)));

		vector<int> indices
		{
			0, 1, 2,
			0, 2, 3,
			4, 5, 6,
			4, 6, 7,
			8, 9, 10,
			8, 10, 11,
			12, 13, 14,
			12, 14, 15,
			16, 17, 18,
			16, 18, 19,
			20, 21, 22,
			20, 22, 23
		};
		cubeMesh.setIndices(move(indices));
	}

	void Scene::initPlaneMesh()
	{
		vector<Vector3> positions
		{
			Vector3(-50, -50, 0),
			Vector3(50, -50, 0),
			Vector3(50, 50, 0),
			Vector3(-50, 50, 0)
		};
		auto positionDataArray = make_unique<VertexDataArray<Vector3>>(positions);
		planeMesh.addData(VertexDataType::Position, static_cast<unique_ptr<IVertexDataArray>>(move(positionDataArray)));

		vector<Vector2> texcoords
		{
			Vector2(0, 0),
			Vector2(1 / 0.8f, 0),
			Vector2(1 / 0.8f, 1 / 0.8f),
			Vector2(0, 1 / 0.8f),
		};
		auto texcoordDataArray = make_unique<VertexDataArray<Vector2>>(texcoords);
		planeMesh.addData(VertexDataType::TexCoord, static_cast<unique_ptr<IVertexDataArray>>(move(texcoordDataArray)));

		vector<Color> colors
		{
			Color::white,
			Color::white,
			Color::white,
			Color::white
		};
		auto colorDataArray = make_unique<VertexDataArray<Color>>(colors);
		planeMesh.addData(VertexDataType::Color, static_cast<unique_ptr<IVertexDataArray>>(move(colorDataArray)));

		vector<Vector3> normals
		{
			Vector3(0, 0, 1),
			Vector3(0, 0, 1),
			Vector3(0, 0, 1),
			Vector3(0, 0, 1)
		};
		auto normalDataArray = make_unique<VertexDataArray<Vector3>>(normals);
		planeMesh.addData(VertexDataType::Normal, static_cast<unique_ptr<IVertexDataArray>>(move(normalDataArray)));

		vector<int> indices
		{
			0, 1, 2,
			0, 2, 3
		};
		planeMesh.setIndices(move(indices));
	}

	void Scene::update(
		float time, float elapsedTime,
		bool up, bool down, bool left, bool right,
		int mouseMotionX, int mouseMotionY)
	{
		//float angle = (fmod(time, 10.0f) / 10.0f) * Math::PI * 2.0f;

		//camera->setPosition(Vector3(30 * cos(angle), 30 * sin(angle), 7));
		//camera->setRotation(Quaternion::eulerAngles(0, 0, Math::PI / 2 + angle));

		if (up || down || left || right)
		{
			float speed = 10;

			auto position = camera->getPosition();

			if (up || down)
			{
				auto forward = camera->getRotation() * Vector3(0, 1, 0);
				position = position + forward * speed * elapsedTime * float((up ? 1 : 0) + (down ? -1 : 0));
			}
			if (left || right)
			{
				auto rightv = camera->getRotation() * Vector3(1, 0, 0);
				position = position + rightv * speed * elapsedTime * float((right ? 1 : 0) + (left ? -1 : 0));
			}

			camera->setPosition(position);
		}
		
		if (mouseMotionX != 0 || mouseMotionY != 0)
		{
			float rotationSpeed = 8.0f;

			auto displacement = Vector3(float(mouseMotionX), float(mouseMotionY), 0);

			int screenSize = max(camera->getSize().x, camera->getSize().y);
			auto screenRelatedDisplacement = displacement / float(screenSize);

			float xAngle = screenRelatedDisplacement.x * rotationSpeed;
			float yAngle = -screenRelatedDisplacement.y * rotationSpeed;

			// in [-PI / 2, PI / 2] range
			float currentYAngle = camera->getRotation().toEulerAngles().x;
			if (currentYAngle > Math::PI)
			{
				currentYAngle = currentYAngle - 2 * Math::PI;
			}
			else if (currentYAngle < -Math::PI)
			{
				currentYAngle = currentYAngle + 2 * Math::PI;
			}

			// 1.48 rad ~= 85 deg
			float newYAngle = Math::clamp(currentYAngle + yAngle, -1.48f, 1.48f);

			camera->rotate(Quaternion::fromEulerAngles(0, 0, -xAngle), true);
			camera->rotate(Quaternion::angleAxis(newYAngle - currentYAngle, Vector3(1, 0, 0)), false);

			auto pos = camera->getPosition();
			auto rot = camera->getRotation();
		}

		shader->setUniform<Vector3>("lightDirection", camera->getViewTransform().multiplyDirection(Vector3(-1, 1, -0.6f).normalized()));
	}

	void Scene::render()
	{
		camera->getRenderTarget().getBuffer<Color>(BufferType::Color).clear(clearColor);
		camera->getRenderTarget().getBuffer<float>(BufferType::Depth).clear(1);

		//triangleRasterizer.render(cubeMesh, Matrix4x4::identity, *camera, *shader);
		triangleRasterizer.render(*mesh, modelTransform, *camera, *shader);
		//triangleRasterizer.render(planeMesh, Matrix4x4::identity, *camera, *shader);
	}

	const Buffer<Color>& Scene::getPixels() const
	{
		return camera->getRenderTarget().getBuffer<Color>(BufferType::Color);
	}
}
