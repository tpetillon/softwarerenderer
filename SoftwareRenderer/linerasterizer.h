#pragma once

namespace sr
{
	class Mesh;
	class Camera;
	class Vector2;
	class Vector3;

	class LineRasterizer
	{
	public:
		void render(Mesh& mesh, Camera& camera) const;

	private:
		void renderLine(int lineIndex, Mesh& mesh, Camera& camera) const;
		void drawPoint(
			int x, int y,
			Vector3 p0vp, Vector3 p1vp, int i0, int i1,
			float interpolation,
			Mesh& mesh, Camera& camera) const;
	};
}
