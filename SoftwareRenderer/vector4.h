#pragma once

namespace sr
{
	class Vector3;
	class Vector4i;

	class Vector4
	{
	public:
		float x;
		float y;
		float z;
		float w;

		Vector4();
		Vector4(float x, float y, float z, float w);
		Vector4(Vector4i v);

		float magnitude() const;
		float sqrtMagnitude() const;

		Vector4 normalized() const;

		Vector3 toVector3() const;

		bool operator==(const Vector4& v) const;
		bool operator!=(const Vector4& v) const;

		Vector4 operator+(const Vector4& v) const;
		Vector4 operator-(const Vector4& v) const;

		Vector4 operator/(float f) const;

		Vector4& operator+=(const Vector4& v);

		float dot(const Vector4& v) const;

		static const Vector4 zero;
		static const Vector4 one;

		static Vector4 lerp(const Vector4& v0, const Vector4& v1, float t);
	};

	Vector4 operator*(const Vector4& v, float f);
	Vector4 operator*(float f, const Vector4& v);
}
