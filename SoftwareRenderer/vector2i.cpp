#include "vector2i.h"

#include "vector2.h"

namespace sr
{
	Vector2i::Vector2i() : Vector2i(0, 0)
	{
	}

	Vector2i::Vector2i(int x, int y) : x(x), y(y)
	{
	}

	Vector2i::Vector2i(Vector2 v) : x(int(v.x)), y(int(v.y))
	{
	}

	int Vector2i::sqrtMagnitude() const
	{
		return x * x + y * y;
	}

	bool Vector2i::operator==(const Vector2i& v) const
	{
		return x == v.x && y == v.y;
	}

	bool Vector2i::operator!=(const Vector2i& v) const
	{
		return !(*this == v);
	}

	Vector2i Vector2i::operator+(const Vector2i& v) const
	{
		return Vector2i(x + v.x, y + v.y);
	}

	Vector2i Vector2i::operator-(const Vector2i& v) const
	{
		return Vector2i(x - v.x, y - v.y);
	}

	Vector2i operator*(const Vector2i& v, int i)
	{
		return Vector2i(v.x * i, v.y * i);
	}

	Vector2i operator*(int i, const Vector2i& v)
	{
		return v * i;
	}

	Vector2i& Vector2i::operator+=(const Vector2i& v)
	{
		x += v.x;
		y += v.y;

		return *this;
	}

	int Vector2i::dot(const Vector2i& v) const
	{
		return x * v.x + y * v.y;
	}

	int Vector2i::cross(const Vector2i& v) const
	{
		return x * v.y - y * v.x;
	}

	const Vector2i Vector2i::zero = Vector2i(0, 0);
	const Vector2i Vector2i::one = Vector2i(1, 1);
}
