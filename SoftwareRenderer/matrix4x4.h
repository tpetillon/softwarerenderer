#pragma once

#include <array>

namespace sr
{
	class Vector3;
	class Vector4;
	class Quaternion;

	// row-major
	// row-major storage
	// column vectors
	// post-multiplication
	//
	// note: It seems that column-major storage could allow for faster vector post-multiplication.
	// 
	// cf. http://seanmiddleditch.com/matrices-handedness-pre-and-post-multiplication-row-vs-column-major-and-notations/
	// cf. http://blog.lotech.org/2013/05/the-beauty-of-using-matricies-to-apply.html
	// cf. http://what-when-how.com/advanced-methods-in-computer-graphics/quaternions-advanced-methods-in-computer-graphics-part-2/

	class Matrix4x4
	{
	public:
		std::array<float, 16> v;

		Matrix4x4();
		Matrix4x4(
			float v00, float v01, float v02, float v03,
			float v10, float v11, float v12, float v13,
			float v20, float v21, float v22, float v23,
			float v30, float v31, float v32, float v33);

		float operator()(unsigned row, unsigned col) const;

		Matrix4x4 operator*(const Matrix4x4& m) const;
		Vector4 operator*(const Vector3& v) const;
		Vector4 operator*(const Vector4& v) const;

		// Transform the vector as a direction, not a point
		Vector3 multiplyDirection(const Vector3& v) const;

		static const Matrix4x4 identity;

		static Matrix4x4 translation(Vector3 translation);
		static Matrix4x4 rotation(Vector3 eulerAngles);
		static Matrix4x4 scale(Vector3 scale);
		static Matrix4x4 transform(Vector3 translation, Quaternion rotation, Vector3 scale);
	};
}
