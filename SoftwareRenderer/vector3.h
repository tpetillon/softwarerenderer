#pragma once

namespace sr
{
	class Vector3
	{
	public:
		float x;
		float y;
		float z;

		Vector3();
		Vector3(float x, float y, float z);

		float magnitude() const;
		float sqrtMagnitude() const;

		Vector3 normalized() const;

		bool operator==(const Vector3& v) const;
		bool operator!=(const Vector3& v) const;

		Vector3 operator+(const Vector3& v) const;
		Vector3 operator-(const Vector3& v) const;

		Vector3 operator/(float f) const;

		Vector3& operator+=(const Vector3& v);

		float dot(const Vector3& v) const;
		Vector3 cross(const Vector3& v) const;

		static Vector3 lerp(const Vector3& v0, const Vector3& v1, float progression);

		static const Vector3 zero;
		static const Vector3 one;
	};

	Vector3 operator*(const Vector3& v, float f);
	Vector3 operator*(float f, const Vector3& v);
}
