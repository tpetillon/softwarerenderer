#include "testshader.h"

#include "vector2.h"
#include "vector3.h"
#include "vector4.h"
#include "color.h"
#include "vertexdatatype.h"
#include "texturesampler.h"
#include "math.h"

#include <cmath>

namespace sr
{
	using std::max;
	using Math::clamp01;

	TestShader::TestShader() : ShaderImpl()
	{
		setUniformOffset("modelViewTransform", offsetof(struct TestShaderUniforms, modelViewTransform));
		setUniformOffset("texture0", offsetof(struct TestShaderUniforms, texture0));
		setUniformOffset("lightDirection", offsetof(struct TestShaderUniforms, lightDirection));
		setUniformOffset("lightIntensity", offsetof(struct TestShaderUniforms, lightIntensity));
		setUniformOffset("ambiantLightIntensity", offsetof(struct TestShaderUniforms, ambiantLightIntensity));

		setVaryingOffset(VertexDataType::Position, offsetof(struct TestShaderVaryings, position));
		//setVaryingOffset(VertexDataType::Color, offsetof(struct TestShaderVaryings, color));
		setVaryingOffset(VertexDataType::TexCoord, offsetof(struct TestShaderVaryings, texCoord));
		setVaryingOffset(VertexDataType::Normal, offsetof(struct TestShaderVaryings, normal));
	}

	Vector4 TestShader::transformVertex(TestShaderVaryings* input)
	{
		return uniforms.modelViewTransform * input->position;
	}

	Color TestShader::colorFragment(TestShaderVaryings* input)
	{
		//auto color = input->color;
		auto color = Color::white;

		auto texCoord = input->texCoord;
		color = uniforms.texture0->getColor(texCoord.x, texCoord.y) * color;

		float lightingIntensity =
			clamp01(max(clamp01(-input->normal.dot(uniforms.lightDirection)), uniforms.ambiantLightIntensity));
		color = color * lightingIntensity;

		return color;
	}
}
