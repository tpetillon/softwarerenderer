#include "texturesampler.h"

#include "samplingtype.h"

#include <cmath>

#include <stdio.h>

namespace sr
{
	using std::round;

	TextureSampler::TextureSampler(int width, int height, Color* data, SamplingType samplingType)
		: width(width), height(height), fwidth(float(width - 1)), fheight(float(height - 1)),
		  data(data), samplingType(samplingType)
	{
	}

	TextureSampler::~TextureSampler()
	{
		delete[] data;
	}

	Color TextureSampler::getColor(float u, float v) const
	{
		// repeat texture
		float uu = fmod(u, 1.0f);
		float vv = fmod(v, 1.0f);

		if (uu < 0.0f)
		{
			uu += 1.0f;
		}
		if (vv < 0.0f)
		{
			vv += 1.0f;
		}

		// nearest neighbour
		int x = int(round(uu * (width - 1)));
		int y = int(round(vv * (height - 1)));

		return data[x + y * width];
	}
}
