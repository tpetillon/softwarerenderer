#pragma once

namespace sr
{
	class Vector3;
	class Matrix4x4;

	class Quaternion
	{
	public:
		float w;
		float x;
		float y;
		float z;

		Quaternion();

		Quaternion operator*(const Quaternion& q) const;
		Vector3 operator*(const Vector3& v) const;

		Quaternion inverse() const;

		Matrix4x4 toRotationMatrix() const;
		Vector3 toEulerAngles() const;

		static Quaternion angleAxis(float angle, Vector3 axis);
		static Quaternion fromEulerAngles(float x, float y, float z);

		static const Quaternion identity;

	private:
		Quaternion(float w, float x, float y, float z);

		void normalize();
	};
}
