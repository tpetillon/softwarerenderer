#pragma once

#include <array>

namespace sr
{
	class IBuffer
	{
	public:
		IBuffer() {}

		virtual ~IBuffer() {}
	};

	template<typename T>
	class Buffer : public IBuffer
	{
	public:
		const size_t width;
		const size_t height;

		Buffer(size_t width, size_t height)
			: width(width), height(height), data(new T[width * height])
		{
		}

		Buffer(const Buffer& other) = delete;

		Buffer(Buffer&& other) : data(other.data)
		{
			other.data = nullptr;
		}

		~Buffer()
		{
			delete[] data;
		}

		T get(size_t x, size_t y) const
		{
			return data[x + width * y];
		}

		void set(size_t x, size_t y, T value)
		{
			data[x + width * y] = value;
		}

		void clear(T clearValue)
		{
			for (size_t i = 0; i < width * height; i++)
			{
				data[i] = clearValue;
			}
		}

	private:
		T* data;
	};
}
