#include "camera.h"

#include "vector2.h"
#include "vector3.h"
#include "vector4.h"
#include "rendertarget.h"
#include "buffer.h"
#include "math.h"

namespace sr
{
	Camera::Camera(
		int width, int height, Vector3 position, Quaternion rotation,
		float fov, float nearPlaneDistance, float farPlaneDistance,
		std::unique_ptr<RenderTarget> renderTarget)
		: cameraWidth(width), cameraHeight(height), size(Vector2i(width, height)),
		nearPlaneDistance(nearPlaneDistance), farPlaneDistance(farPlaneDistance), cameraFoV(fov),
		position(position), rotation(rotation),
		renderTarget(std::move(renderTarget))
	{
		aspectRatio = static_cast<float>(cameraWidth) / static_cast<float>(cameraHeight);
		horizontalFoV = 2 * atan(aspectRatio * tan(cameraFoV / 2));

		computeProjectionMatrix();
		computeTransform();
		computeViewTransform();
		computeForwardVector();
	}

	void Camera::computeTransform()
	{
		transform = Matrix4x4::transform(position, rotation, Vector3::one);
	}

	void Camera::computeViewTransform()
	{
		auto axisRotation = Matrix4x4::rotation(Vector3{ -Math::PI / 2, 0, 0 }); // put z on depth axis

		viewTransform = axisRotation * rotation.inverse().toRotationMatrix() * Matrix4x4::translation(position * -1);

		viewProjection = projection * viewTransform;
	}

	void Camera::computeProjectionMatrix()
	{
		// cf. http://www.songho.ca/opengl/gl_projectionmatrix.html

		float a = -(farPlaneDistance + nearPlaneDistance) / (farPlaneDistance - nearPlaneDistance);
		float b = -(2.0f * farPlaneDistance * nearPlaneDistance) / (farPlaneDistance - nearPlaneDistance);

		//FIXME
		// My calculations give me "1 / (aspectRatio * tan(cameraFoV / 2))" instead of
		// "aspectRatio / tan(cameraFoV / 2)", but the latter and not the former gives the
		// correct results. So either the calculations are wrong, or something else is.

		projection = Matrix4x4(
			1 / tan(cameraFoV / 2), 0, 0, 0,
			0, aspectRatio / tan(cameraFoV / 2), 0, 0,
			0, 0, a, b,
			0, 0, -1, 0);
	}

	void Camera::computeForwardVector()
	{
		// At (0, 0, 0) rotation, the camera looks towards +y
		forward = (transform * Vector4(0, 1, 0, 0)).toVector3();
	}

	void Camera::setPosition(Vector3 position)
	{
		this->position = position;

		computeTransform();
		computeViewTransform();
		computeForwardVector();
	}

	void Camera::setRotation(Quaternion rotation)
	{
		this->rotation = rotation;

		computeTransform();
		computeViewTransform();
		computeForwardVector();
	}

	void Camera::translate(Vector3 translation)
	{
		position = position + translation;

		computeTransform();
		computeViewTransform();
		computeForwardVector();
	}

	void Camera::rotate(Quaternion rotation, bool inWorldSpace)
	{
		if (inWorldSpace)
		{
			this->rotation = rotation * this->rotation;
		}
		else
		{
			this->rotation = this->rotation * rotation;
		}

		computeTransform();
		computeViewTransform();
		computeForwardVector();
	}

	Vector3 Camera::transformWorldToViewport(Vector3 position) const
	{
		auto v = projection * (viewTransform * position);
		return Vector3(v.x / v.w, v.y / v.w, v.z / v.w);
	}

	Vector2 Camera::transformViewportToScreen(Vector3 position) const
	{
		return Vector2(
			(position.x * size.x) / 2.0f + size.x / 2.0f,
			(position.y * size.y) / 2.0f + size.y / 2.0f);
	}

	Vector2i Camera::transformViewportToScreen(Vector3 position, int subpixelPrecision) const
	{
		return Vector2i(
			int(position.x * size.x * subpixelPrecision) / 2 + (size.x * subpixelPrecision) / 2,
			int(position.y * size.y * subpixelPrecision) / 2 + (size.y * subpixelPrecision) / 2);
	}

	bool Camera::clipped(int x, int y) const
	{
		return x < 0 || x >= size.x || y < 0 || y >= size.y;
	}
}
