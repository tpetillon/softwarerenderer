#pragma once

namespace sr
{
	class Color
	{
	public:
		float r;
		float g;
		float b;
		float a;

		Color();
		Color(float r, float g, float b, float a);
		Color(float r, float g, float b);

		bool operator==(const Color& c) const;
		bool operator!=(const Color& c) const;

		Color operator+(const Color& c) const;
		Color operator-(const Color& c) const;

		Color operator*(const Color& c) const;

		Color operator/(float f) const;

		static Color lerp(const Color& c0, const Color& c1, float progression);

		static const Color white;
		static const Color black;
		static const Color red;
		static const Color green;
		static const Color blue;
		static const Color yellow;
		static const Color cyan;
		static const Color magenta;
		static const Color grey;
	};

	Color operator*(const Color& c, float f);
	Color operator*(float f, const Color& c);
}
