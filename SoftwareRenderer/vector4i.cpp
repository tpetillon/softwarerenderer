#include "vector4i.h"

#include "vector4.h"

namespace sr
{
	Vector4i::Vector4i() : Vector4i(0, 0, 0, 0)
	{
	}

	Vector4i::Vector4i(int x, int y, int z, int w) : x(x), y(y), z(z), w(w)
	{
	}

	Vector4i::Vector4i(Vector4 v) : x(int(v.x)), y(int(v.y)), z(int(v.z)), w(int(v.w))
	{
	}

	int Vector4i::sqrtMagnitude() const
	{
		return x * x + y * y + z * z + w * w;
	}

	bool Vector4i::operator==(const Vector4i& v) const
	{
		return x == v.x && y == v.y && z == v.z && w == v.w;
	}

	bool Vector4i::operator!=(const Vector4i& v) const
	{
		return !(*this == v);
	}

	Vector4i Vector4i::operator+(const Vector4i& v) const
	{
		return Vector4i(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	Vector4i Vector4i::operator-(const Vector4i& v) const
	{
		return Vector4i(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	Vector4i Vector4i::operator+(int i) const
	{
		return Vector4i(x + i, y + i, z + i, w + i);
	}

	Vector4i Vector4i::operator/(int i) const
	{
		return Vector4i(x / i, y / i, z / i, w / i);
	}

	Vector4i operator*(const Vector4i& v, int i)
	{
		return Vector4i(v.x * i, v.y * i, v.z * i, v.w * i);
	}

	Vector4i operator*(int i, const Vector4i& v)
	{
		return v * i;
	}

	Vector4i& Vector4i::operator+=(const Vector4i& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;

		return *this;
	}

	Vector4i& Vector4i::operator+=(int i)
	{
		x += i;
		y += i;
		z += i;
		w += i;

		return *this;
	}

	Vector4i Vector4i::operator|(const Vector4i& v)
	{
		return Vector4i(x | v.x, y | v.y, z | v.z, w | v.w);
	}

	int Vector4i::dot(const Vector4i& v) const
	{
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	const Vector4i Vector4i::zero = Vector4i(0, 0, 0, 0);
	const Vector4i Vector4i::one = Vector4i(1, 1, 1, 1);
}
