#include "trianglerasterizer.h"

#include "mesh.h"
#include "camera.h"
#include "meshtype.h"
#include "vector2.h"
#include "vector3.h"
#include "matrix4x4.h"
#include "color.h"
#include "shader.h"
#include "vertexdatatype.h"

#include <cmath>

namespace sr
{
	using std::min;
	using std::max;

	// Pixel pattern:
	// +---+---+
	// | 2 | 3 |
	// +---+---+
	// | 0 | 1 |
	// +---+---+
	TriangleRasterizer::TriangleRasterizer() :
		subpixelPrecision(1 << 8),
		pixelGroupPatternX(0, 1, 0, 1),
		pixelGroupPatternY(0, 0, 1, 1),
		pixelGroupSize(Vector2i(2, 2))
	{
	}

	void TriangleRasterizer::render(Mesh& mesh, const Matrix4x4& transform, Camera& camera, Shader& shader) const
	{
		if (mesh.getType() != MeshType::Triangles)
		{
			// Error, mesh type not supported
			return;
		}

		shader.setUniform<Matrix4x4>("modelViewTransform", camera.getViewProjection() * transform);

		for (size_t i = 0; i < mesh.getIndices().size() / 3; i++)
		{
			renderTriangle(i, mesh, camera, shader);
		}
	}

	void TriangleRasterizer::renderTriangle(int triangleIndex, Mesh& mesh, Camera& camera, Shader& shader) const
	{
		int i0 = mesh.getIndices()[triangleIndex * 3];
		int i1 = mesh.getIndices()[triangleIndex * 3 + 1];
		int i2 = mesh.getIndices()[triangleIndex * 3 + 2];

		TriangleData data0, data1;
		data0.var0 = shader.getVaryingStruct(0);
		data0.var1 = shader.getVaryingStruct(1);
		data0.var2 = shader.getVaryingStruct(2);

		getVaryings(i0, data0.var0, mesh, camera, shader);
		getVaryings(i1, data0.var1, mesh, camera, shader);
		getVaryings(i2, data0.var2, mesh, camera, shader);

		auto p0 = shader.transformVertex(data0.var0);
		auto p1 = shader.transformVertex(data0.var1);
		auto p2 = shader.transformVertex(data0.var2);

		if (!inViewFrustum(p0, p1, p2))
		{
			// Discard
			return;
		}

		// clip
		bool split = clipTriangle(p0, p1, p2, data0, data1, mesh, shader);

		renderClippedTriangle(data0, i0, i1, i2, mesh, camera, shader);

		if (split)
		{
			renderClippedTriangle(data1, i0, i1, i2, mesh, camera, shader);
		}
	}

	void TriangleRasterizer::getVaryings(int vertexIndex, void* varyingStruct, Mesh& mesh, Camera& camera, Shader& shader) const
	{
		for (auto varying : shader.getVaryingOffsets())
		{
			VertexDataType type = varying.first;
			uintptr_t offset = varying.second;

			switch (type)
			{
			case VertexDataType::Position:
			{
				Vector3 position = mesh.getData<Vector3>(VertexDataType::Position)[vertexIndex];
				*(Vector3*)((char*)varyingStruct + offset) = position;
				break;
			}
			case VertexDataType::Color:
			{
				Color color = mesh.getData<Color>(VertexDataType::Color)[vertexIndex];
				*(Color*)((char*)varyingStruct + offset) = color;
				break;
			}
			case VertexDataType::TexCoord:
			{
				Vector2 texCoord = mesh.getData<Vector2>(VertexDataType::TexCoord)[vertexIndex];
				*(Vector2*)((char*)varyingStruct + offset) = texCoord;
				break;
			}
			case VertexDataType::Normal:
			{
				Vector3 normal = mesh.getData<Vector3>(VertexDataType::Normal)[vertexIndex];
				normal = camera.getViewTransform().multiplyDirection(normal);
				*(Vector3*)((char*)varyingStruct + offset) = normal;
				break;
			}
			}
		}
	}

	void* TriangleRasterizer::interpolateVaryings(void* var0, void* var1, void* newVar, float progression, Mesh& mesh, Shader& shader) const
	{
		for (auto varying : shader.getVaryingOffsets())
		{
			VertexDataType type = varying.first;
			uintptr_t offset = varying.second;

			switch (type)
			{
			case VertexDataType::Position:
			{
				Vector3 position0 = *(Vector3*)((char*)var0 + offset);
				Vector3 position1 = *(Vector3*)((char*)var1 + offset);
				*(Vector3*)((char*)newVar + offset) = Vector3::lerp(position0, position1, progression);
				break;
			}
			case VertexDataType::Color:
			{
				Color color0 = *(Color*)((char*)var0 + offset);
				Color color1 = *(Color*)((char*)var1 + offset);
				*(Color*)((char*)newVar + offset) = Color::lerp(color0, color1, progression);
				break;
			}
			case VertexDataType::TexCoord:
			{
				Vector2 texCoord0 = *(Vector2*)((char*)var0 + offset);
				Vector2 texCoord1 = *(Vector2*)((char*)var1 + offset);
				*(Vector2*)((char*)newVar + offset) = Vector2::lerp(texCoord0, texCoord1, progression);
				break;
			}
			case VertexDataType::Normal:
			{
				Vector3 normal0 = *(Vector3*)((char*)var0 + offset);
				Vector3 normal1 = *(Vector3*)((char*)var1 + offset);
				*(Vector3*)((char*)newVar + offset) = Vector3::lerp(normal0, normal1, progression);
				break;
			}
			}
		}

		return newVar;
	}

	void* TriangleRasterizer::interpolateVaryings(
		void* var0, void* var1, void* var2, void* newVar,
		float weight0, float weight1, float weight2,
		Mesh& mesh, Shader& shader) const
	{
		for (auto varying : shader.getVaryingOffsets())
		{
			VertexDataType type = varying.first;
			uintptr_t offset = varying.second;

			switch (type)
			{
			case VertexDataType::Position:
			{
				Vector3 position0 = *(Vector3*)((char*)var0 + offset);
				Vector3 position1 = *(Vector3*)((char*)var1 + offset);
				Vector3 position2 = *(Vector3*)((char*)var2 + offset);
				*(Vector3*)((char*)newVar + offset) = position0 * weight0 + position1 * weight1 + position2 * weight2;
				break;
			}
			case VertexDataType::Color:
			{
				Color color0 = *(Color*)((char*)var0 + offset);
				Color color1 = *(Color*)((char*)var1 + offset);
				Color color2 = *(Color*)((char*)var2 + offset);
				*(Color*)((char*)newVar + offset) = color0 * weight0 + color1 * weight1 + color2 * weight2;
				break;
			}
			case VertexDataType::TexCoord:
			{
				Vector2 texCoord0 = *(Vector2*)((char*)var0 + offset);
				Vector2 texCoord1 = *(Vector2*)((char*)var1 + offset);
				Vector2 texCoord2 = *(Vector2*)((char*)var2 + offset);
				*(Vector2*)((char*)newVar + offset) = texCoord0 * weight0 + texCoord1 * weight1 + texCoord2 * weight2;
				break;
			}
			case VertexDataType::Normal:
			{
				Vector3 normal0 = *(Vector3*)((char*)var0 + offset);
				Vector3 normal1 = *(Vector3*)((char*)var1 + offset);
				Vector3 normal2 = *(Vector3*)((char*)var2 + offset);
				*(Vector3*)((char*)newVar + offset) = normal0 * weight0 + normal1 * weight1 + normal2 * weight2;
				break;
			}
			}
		}

		return newVar;
	}
	
	bool TriangleRasterizer::clipTriangle(Vector4 p0, Vector4 p1, Vector4 p2, TriangleData& data0, TriangleData& data1, Mesh& mesh, Shader& shader) const
	{
		// Return true if the triangle has been split into two new triangles

		bool clip0 = p0.z < -p0.w;
		bool clip1 = p1.z < -p1.w;
		bool clip2 = p2.z < -p2.w;

		if (clip0 && clip1 && !clip2)
		{
			float t0 = clipVertex(p0, p2);
			auto newp0 = Vector4::lerp(p0, p2, t0);
			data0.p0 = newp0;
			data0.var0 = interpolateVaryings(data0.var0, data0.var2, shader.getVaryingStruct(3), t0, mesh, shader);

			float t1 = clipVertex(p1, p2);
			auto newp1 = Vector4::lerp(p1, p2, t1);
			data0.p1 = newp1;
			data0.var1 = interpolateVaryings(data0.var1, data0.var2, shader.getVaryingStruct(4), t1, mesh, shader);

			data0.p2 = p2;

			return false;
		}
		else if (clip0 && !clip1 && clip2)
		{
			float t0 = clipVertex(p0, p1);
			auto newp0 = Vector4::lerp(p0, p1, t0);
			data0.p0 = newp0;
			data0.var0 = interpolateVaryings(data0.var0, data0.var1, shader.getVaryingStruct(3), t0, mesh, shader);

			data0.p1 = p1;

			float t2 = clipVertex(p2, p1);
			auto newp2 = Vector4::lerp(p2, p1, t2);
			data0.p2 = newp2;
			data0.var2 = interpolateVaryings(data0.var2, data0.var1, shader.getVaryingStruct(4), t2, mesh, shader);

			return false;
		}
		else if (!clip0 && clip1 && clip2)
		{
			data0.p0 = p0;

			float t1 = clipVertex(p1, p0);
			auto newp1 = Vector4::lerp(p1, p0, t1);
			data0.p1 = newp1;
			data0.var1 = interpolateVaryings(data0.var1, data0.var0, shader.getVaryingStruct(3), t1, mesh, shader);

			float t2 = clipVertex(p2, p0);
			auto newp2 = Vector4::lerp(p2, p0, t2);
			data0.p2 = newp2;
			data0.var2 = interpolateVaryings(data0.var2, data0.var0, shader.getVaryingStruct(4), t2, mesh, shader);

			return false;
		}
		else if (clip0 && !clip1 && !clip2) // ok
		{
			float t01 = clipVertex(p0, p1);
			auto newp01 = Vector4::lerp(p0, p1, t01);
			auto newVar01 = interpolateVaryings(data0.var0, data0.var1, shader.getVaryingStruct(3), t01, mesh, shader);

			float t02 = clipVertex(p0, p2);
			auto newp02 = Vector4::lerp(p0, p2, t02);
			auto newVar02 = interpolateVaryings(data0.var0, data0.var2, shader.getVaryingStruct(4), t02, mesh, shader);

			data0.p0 = newp01;
			data0.var0 = newVar01;

			data0.p1 = p1;

			data0.p2 = p2;

			data1.p0 = newp01;
			data1.var0 = newVar01;

			data1.p1 = p2;
			data1.var1 = data0.var2;

			data1.p2 = newp02;
			data1.var2 = newVar02;

			return true;
		}
		else if (!clip0 && clip1 && !clip2)
		{
			float t10 = clipVertex(p1, p0);
			auto newp10 = Vector4::lerp(p1, p0, t10);
			auto newVar10 = interpolateVaryings(data0.var1, data0.var0, shader.getVaryingStruct(3), t10, mesh, shader);

			float t12 = clipVertex(p1, p2);
			auto newp12 = Vector4::lerp(p1, p2, t12);
			auto newVar12 = interpolateVaryings(data0.var1, data0.var2, shader.getVaryingStruct(4), t12, mesh, shader);

			data0.p0 = p0;

			data0.p1 = newp12;
			data0.var1 = newVar12;

			data0.p2 = p2;

			data1.p0 = p0;
			data1.var0 = data0.var0;

			data1.p1 = newp10;
			data1.var1 = newVar10;

			data1.p2 = newp12;
			data1.var2 = newVar12;

			return true;
		}
		else if (!clip0 && !clip1 && clip2)
		{
			float t20 = clipVertex(p2, p0);
			auto newp20 = Vector4::lerp(p2, p0, t20);
			auto newVar20 = interpolateVaryings(data0.var2, data0.var0, shader.getVaryingStruct(3), t20, mesh, shader);

			float t21 = clipVertex(p2, p1);
			auto newp21 = Vector4::lerp(p2, p1, t21);
			auto newVar21 = interpolateVaryings(data0.var2, data0.var1, shader.getVaryingStruct(4), t21, mesh, shader);

			data0.p0 = p0;

			data0.p1 = p1;

			data0.p2 = newp20;
			data0.var2 = newVar20;

			data1.p0 = newp20;
			data1.var0 = newVar20;

			data1.p1 = p1;
			data1.var1 = data0.var1;

			data1.p2 = newp21;
			data1.var2 = newVar21;

			return true;
		}
		else
		{
			// triangle is not clipped
			// should not end up here if proper culling has been done before

			data0.p0 = p0;
			data0.p1 = p1;
			data0.p2 = p2;

			return false;
		}
	}

	float TriangleRasterizer::clipVertex(Vector4 p0, Vector4 p1) const
	{
		// Compute { t | p = p0 + t(p1 - p0)), p.z == -p.w }
		// cf. http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		return (-p0.z - p0.w) / (p1.z - p0.z + p1.w - p0.w);
	}

	TriangleRasterizer::Edge::Edge(const Vector2i& p0, const Vector2i& p1, const Vector2i& minP, int subpixelPrecision,
		const Vector4i& pixelGroupPatternX, const Vector4i& pixelGroupPatternY, const Vector2i& pixelGroupSize)
	{
		// Fill-rule bias
		int bias = isTopLeftEdge(p0, p1) ? 0 : -1;

		// Triangle setup
		// These values are components of the parallelogramArea() operation.
		// (See parallelogramArea() comments.)
		// As we always move in whole pixel steps, that is, constant steps,
		// we can avoid recomputing parallelogramArea() fully for each pixel
		// by adding these increments.
		// (onePixelxIncrement is a, onePixelyIncrement is b from parallelogramArea() comments.)
		int onePixelxIncrement = p0.y - p1.y;
		int onePixelyIncrement = p1.x - p0.x;

		// Barycentric coordinates at minX/minY corner
		int64_t areaToMinP = parallelogramArea(p0, p1, minP) + bias;

		// The weights and the increments are divided by the
		// subpixel precision to fit in 32-bit integers.
		// cf. https://fgiesen.wordpress.com/2013/02/08/triangle-rasterization-in-practice/#comment-3751
		// (The increments are not actually divided, they are just not
		// multiplied.)
		int actualMinPWeight = int(areaToMinP / subpixelPrecision);

		minPWeight = Vector4i(
			actualMinPWeight + onePixelyIncrement * pixelGroupPatternY.x + onePixelxIncrement * pixelGroupPatternX.x,
			actualMinPWeight + onePixelyIncrement * pixelGroupPatternY.y + onePixelxIncrement * pixelGroupPatternX.y,
			actualMinPWeight + onePixelyIncrement * pixelGroupPatternY.z + onePixelxIncrement * pixelGroupPatternX.z,
			actualMinPWeight + onePixelyIncrement * pixelGroupPatternY.w + onePixelxIncrement * pixelGroupPatternX.w);

		// xIncrement and yIncrement are for one pixel pattern at a time
		xIncrement = onePixelxIncrement * pixelGroupSize.x;
		yIncrement = onePixelyIncrement * pixelGroupSize.y;
	}
	
	void TriangleRasterizer::renderClippedTriangle(
		TriangleData& data, int i0, int i1, int i2,
		Mesh& mesh, Camera& camera, Shader& shader) const
	{
		auto p0ndc = perspectiveDivide(data.p0);
		auto p1ndc = perspectiveDivide(data.p1);
		auto p2ndc = perspectiveDivide(data.p2);

		auto p0s = camera.transformViewportToScreen(p0ndc, subpixelPrecision);
		auto p1s = camera.transformViewportToScreen(p1ndc, subpixelPrecision);
		auto p2s = camera.transformViewportToScreen(p2ndc, subpixelPrecision);

		if ((p1s - p0s).cross(p2s - p0s) <= 0)
		{
			// Triangle facing the opposite direction, discarded
			return;
		}

		float area = float(parallelogramArea(p0s, p1s, p2s)); // actually 2 * area
		area = area / float(subpixelPrecision); // all the weights are divided by subpixelPrecision

		int minX = int(max(0, min({ p0s.x, p1s.x, p2s.x })));
		int maxX = int(min(camera.getSize().x * subpixelPrecision - 1, max({ p0s.x, p1s.x, p2s.x })));
		int minY = int(max(0, min({ p0s.y, p1s.y, p2s.y })));
		int maxY = int(min(camera.getSize().y * subpixelPrecision - 1, max({ p0s.y, p1s.y, p2s.y })));

		// Snap bounding box to whole pixels patterns
		// (Pixel patterns and not simple pixels in order to have a better memory alignment)
		minX = minX - (minX % (subpixelPrecision * pixelGroupSize.x));
		minY = minY - (minY % (subpixelPrecision * pixelGroupSize.y));

		auto minP = Vector2i(minX, minY);

		const auto edge01 = Edge(p0s, p1s, minP, subpixelPrecision, pixelGroupPatternX, pixelGroupPatternY, pixelGroupSize);
		const auto edge12 = Edge(p1s, p2s, minP, subpixelPrecision, pixelGroupPatternX, pixelGroupPatternY, pixelGroupSize);
		const auto edge20 = Edge(p2s, p0s, minP, subpixelPrecision, pixelGroupPatternX, pixelGroupPatternY, pixelGroupSize);

		Vector4i w01Row = edge01.minPWeight;
		Vector4i w12Row = edge12.minPWeight;
		Vector4i w20Row = edge20.minPWeight;

		// Rasterise
		// Move by subpixelPrecision steps, that is whole pixels at a time
		for (int y = minY; y <= maxY; y += subpixelPrecision * pixelGroupSize.y)
		{
			// Barycentric coordinates at start of row
			Vector4i w01 = w01Row;
			Vector4i w12 = w12Row;
			Vector4i w20 = w20Row;

			for (int x = minX; x <= maxX; x += subpixelPrecision * pixelGroupSize.x)
			{
				// A pixel is in the triangle if w01 >= 0 && w12 >= 0 && w20 >= 0
				// In integer binary representation, testing a >= 0 && b >= 0 && c >= 0
				// is the same as testing (a | b | c) >= 0
				Vector4i mask = w01 | w12 | w20;

				// If p is on or inside all edges for any pixel, render pixels
				if (hasNonNegativeComponent(mask))
				{
					Vector4 weight0 = Vector4(w12) / area;
					Vector4 weight1 = Vector4(w20) / area;
					Vector4 weight2 = Vector4(w01) / area;

					Vector4i xCoordinates = pixelGroupPatternX + x / subpixelPrecision;
					Vector4i yCoordinates = pixelGroupPatternY + y / subpixelPrecision;

					drawPoints(
						mask,
						xCoordinates, yCoordinates,
						data.p0, data.p1, data.p2,
						p0ndc.z, p1ndc.z, p2ndc.z,
						i0, i1, i2,
						weight0, weight1, weight2,
						data,
						mesh, camera, shader);
				}

				// One step to the right
				w01 += edge01.xIncrement;
				w12 += edge12.xIncrement;
				w20 += edge20.xIncrement;
			}

			// One row step
			w01Row += edge01.yIncrement;
			w12Row += edge12.yIncrement;
			w20Row += edge20.yIncrement;
		}
	}

	void TriangleRasterizer::drawPoints(
		Vector4i mask,
		Vector4i x, Vector4i y,
		Vector4 p0, Vector4 p1, Vector4 p2,
		float z0, float z1, float z2,
		int i0, int i1, int i2,
		Vector4 weight0, Vector4 weight1, Vector4 weight2,
		TriangleData& data,
		Mesh& mesh, Camera& camera, Shader& shader) const
	{
		if (mask.x >= 0 && mask.y >= 0 && mask.z >= 0 && mask.w >= 0)
		{
			int t = 0;
			t++;
		}
		if (mask.x >= 0)
		{
			drawPoint(x.x, y.x, p0, p1, p2, z0, z1, z2, i0, i1, i2, weight0.x, weight1.x, weight2.x, data, mesh, camera, shader);
		}
		if (mask.y >= 0)
		{
			drawPoint(x.y, y.y, p0, p1, p2, z0, z1, z2, i0, i1, i2, weight0.y, weight1.y, weight2.y, data, mesh, camera, shader);
		}
		if (mask.z >= 0)
		{
			drawPoint(x.z, y.z, p0, p1, p2, z0, z1, z2, i0, i1, i2, weight0.z, weight1.z, weight2.z, data, mesh, camera, shader);
		}
		if (mask.w >= 0)
		{
			drawPoint(x.w, y.w, p0, p1, p2, z0, z1, z2, i0, i1, i2, weight0.w, weight1.w, weight2.w, data, mesh, camera, shader);
		}
	}

	void TriangleRasterizer::drawPoint(
		int x, int y,
		Vector4 p0, Vector4 p1, Vector4 p2,
		float z0, float z1, float z2,
		int i0, int i1, int i2,
		float weight0, float weight1, float weight2,
		TriangleData& data,
		Mesh& mesh, Camera& camera, Shader& shader) const
	{
		if (camera.clipped(x, y))
		{
			return;
		}

		auto depth = weight0 * z0 + weight1 * z1 + weight2 * z2;

		if (depth < -1 || depth > 1)
		{
			return;
		}

		auto depthBufferValue = camera.getRenderTarget().getBuffer<float>(BufferType::Depth).get(x, y);

		if (depth > depthBufferValue)
		{
			return;
		}

		camera.getRenderTarget().getBuffer<float>(BufferType::Depth).set(x, y, depth);

		// Perspective correction
		float w = 1 / ((1 / p0.w) * weight0 + (1 / p1.w) * weight1 + (1 / p2.w) * weight2);

		auto varyings = interpolateVaryings(
			data.var0, data.var1, data.var2, shader.getVaryingStruct(5),
			(weight0 / p0.w) * w, (weight1 / p1.w) * w, (weight2 / p2.w) * w,
			mesh, shader);
		auto color = shader.colorFragment(varyings);

		camera.getRenderTarget().getBuffer<Color>(BufferType::Color).set(x, y, color);
	}

	Vector3 TriangleRasterizer::perspectiveDivide(const Vector4& v)
	{
		return Vector3(v.x / v.w, v.y / v.w, v.z / v.w);
	}

	bool TriangleRasterizer::inViewFrustum(const Vector4& p0, const Vector4& p1, const Vector4& p2)
	{
		return !(
			(p0.x < -p0.w && p1.x < -p1.w && p2.x < -p2.w) ||
			(p0.x > p0.w && p1.x > p1.w && p2.x > p2.w) ||
			(p0.y < -p0.w && p1.y < -p1.w && p2.y < -p2.w) ||
			(p0.y > p0.w && p1.y > p1.w && p2.y > p2.w) ||
			(p0.z < -p0.w && p1.z < -p1.w && p2.z < -p2.w) ||
			(p0.z > p0.w && p1.z > p1.w && p2.z > p2.w)) ||
			(p0.w <= 0 && p1.w <= 0 && p2.w <= 0);
	}

	// Return the signed area of the parallelogram swept by p0p1 and p0p2
	// < 0: p2 is on the right of p0p1
	// 0: on the line
	// > 0: on the left
	int64_t TriangleRasterizer::parallelogramArea(const Vector2i& p0, const Vector2i& p1, const Vector2i& p2)
	{
		return
			(int64_t(p1.x) - int64_t(p0.x)) * (int64_t(p2.y) - int64_t(p0.y)) -
			(int64_t(p1.y) - int64_t(p0.y)) * (int64_t(p2.x) - int64_t(p0.x));

		// The operation can be rewritten as such (with p = p2):
		// (p1x - p0x) * (py - p0y) - (p1y - p0y) * (px - p0x)
		// p1x * py - p1x * p0y - p0x * py + p0x * p0y - (p1y * px - p1y * p0x - p0y * px + p0y * p0x
		// p1x * py - p1x * p0y - p0x * py + p0x * p0y - p1y * px + p1y * p0x + p0y * px - p0y * p0x
		// (p0y - p1y) * px + (p1x * - p0x) * py + (p0x * p1y - p0y * p1x)
		// a * px + b * py + c
		// with a = p0y - p1y
		//      b = p1x * - p0x
		//      c = p0x * p1y - p0y * p1x
		// Therefore
		// area(p0, p1, {x + 1, y}) == area(p0, p1, {x, y}) + a
		// area(p0, p1, {x, y + 1}) == area(p0, p1, {x, y}) + b
	}

	bool TriangleRasterizer::isTopLeftEdge(const Vector2i& p0, const Vector2i& p1)
	{
		return
			(p0.y == p1.y && p1.x < p0.x) || // top edge
			(p1.y < p0.y); // left edge
	}

	bool TriangleRasterizer::hasNonNegativeComponent(const Vector4i& v)
	{
		return v.x >= 0 || v.y >= 0 || v.z >= 0 || v.w >= 0;
	}

	TriangleRasterizer::VertexWeights::VertexWeights(float w0, float w1, float w2) : w0(w0), w1(w1), w2(w2)
	{
	}
}
