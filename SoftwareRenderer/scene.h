#pragma once

#include "color.h"
#include "camera.h"
#include "mesh.h"
#include "linerasterizer.h"
#include "trianglerasterizer.h"
#include "texturesampler.h"
#include "shader.h"
#include "matrix4x4.h"

#include <memory>
#include <vector>

namespace sr
{
	template <class T> class Buffer;

	class Scene
	{
	public:
		Scene(int screenWidth, int screenHeight,
			std::unique_ptr<Mesh> mesh, Matrix4x4 modelTransform,
			std::unique_ptr<TextureSampler> texture);

		void update(
			float time, float elapsedTime, // in seconds
			bool up, bool down, bool left, bool right,
			int mouseMotionX, int mouseMotionY);

		void render();

		const Buffer<Color>& getPixels() const;

	private:
		std::unique_ptr<Camera> camera;

		LineRasterizer lineRasterizer;
		TriangleRasterizer triangleRasterizer;

		Color clearColor;
		std::unique_ptr<Mesh> mesh;
		Matrix4x4 modelTransform;
		Mesh cubeMesh;
		Mesh planeMesh;
		std::unique_ptr<TextureSampler> texture;
		std::unique_ptr<Shader> shader;

		void initCubeMesh();
		void initPlaneMesh();
	};
}
