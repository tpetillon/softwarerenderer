# Software 3D renderer

[Homepage](http://www.thom.space/projects/softwarerenderer/)

A simple software 3D renderer written in C++.

Features:
* Vertex arrays,
* Z-buffer,
* Triangle clipping,
* Perspective-correct interpolation,
* Shader system (vertex and fragment),
* Texture sampling,
* Pixels drawn in quads,
* OBJ file loading,
* FPS-style camera.

Third-party libraries are not used, with the notable exception of [SDL](https://www.libsdl.org/), which is used for obtaining a drawable surface as well as user input.

A Visual Studio 2013 project is provided in the repository.

Licence: [MIT](https://opensource.org/licenses/MIT)
